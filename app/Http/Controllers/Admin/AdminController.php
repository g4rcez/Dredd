<?php namespace dredd\Http\Controllers\Admin;

use dredd\Http\Controllers\Controller;
use dredd\Models\Denuncia;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index');
    }

    public function relatorio()
    {
        return view('admin.relatorio');
    }

    public function controleAgente()
    {
        return view('admin.users.index');
    }

    public function ocorrencias()
    {
        $denuncias = Denuncia::paginate(10);
        return view('admin.listar_ocorrencias', compact('denuncias'));
    }
}
