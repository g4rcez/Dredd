<?php namespace dredd\Http\Controllers\Admin;

use dredd\Models\Crimes;
use dredd\Http\Controllers\Controller;
use dredd\Http\Requests\Admin\ControleDenunciaRequest;

class CrudDenunciaController extends Controller
{
    private $crudDenuncia;

    public function __construct(Crimes $crudDenuncia)
    {
        $this->crudDenuncia= $crudDenuncia;
    }

    public function index(){
        return view('admin.tipo_denuncia.index');
    }

    public function view(){
        return view('admin.tipo_denuncia.adicionar');
    }

    public function create(ControleDenunciaRequest $request){
        $this->crudDenuncia->fill($request->all());
        if (!$this->crudDenuncia->save()) {
            return view('errors.503');
        }
        return redirect()->back()->withErrors();
    }

    public function update(AcompanhamentoRequest $request){
        $this->crudDenuncia->where('titulo', $request->only('titulo'));
        if (!$this->crudDenuncia->save()) {
            return view('errors.503');
        }
        return redirect()->back()->withErrors();
    }
}
