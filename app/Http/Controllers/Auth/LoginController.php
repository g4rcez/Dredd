<?php namespace dredd\Http\Controllers\Auth;

use dredd\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/dredd/_giforseg/dashboard';
    protected $redirectTo = '/dredd/_giforseg/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm() //definir o argumento login para criar gerenciamento de usuário->rota
    {
        return view("/admin/auth/login");
    }

    public function username()
    {
        return 'login';
    }

    public function forgot()
    {
        return view('index');
    }
}
