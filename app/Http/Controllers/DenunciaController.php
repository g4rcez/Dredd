<?php namespace dredd\Http\Controllers;
use Carbon\Carbon;
use dredd\Models\Crimes;
use dredd\Models\Denuncia;
use Illuminate\Support\Facades\Hash;
use Canducci\ZipCode\Facades\ZipCode;
use dredd\Http\Requests\OcorrenciaRequest;
use dredd\Http\Requests\AcompanhamentoRequest;

class DenunciaController extends Controller
{
    private $denuncia;

    public function __construct(Denuncia $denuncia)
    {
        $this->denuncia = $denuncia;
    }

    public function denunciar()
    {
        $tipoOcorrencia = Crimes::all();
        return view('denuncia.denunciar')->with('tipoOcorrencia', $tipoOcorrencia);
    }

    /**
     * @param OcorrenciaRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function encaminharOcorrencia(OcorrenciaRequest $request)
    {
        $this->denuncia->fill($request->all());
        $horaDataPreFormatado = $request->input('data') . ' ' . $request->input('hora');
        $this->denuncia->data_hora = Carbon::createFromFormat('d/m/Y G:i', $horaDataPreFormatado, 'America/Sao_Paulo');
        $this->_setValuesFromCep();
        $this->denuncia->protocolo = $this->denuncia->setProtocolo();
        $this->denuncia->password = bcrypt($this->denuncia->password);

        if (!$this->denuncia->save()) {
            abort(503, 'Erro ao salvar denúncia.');
        }
        $this->_saveFiles($request->arquivos);
        $denunciasRelacionadas = $request->input('tipos_denuncia_id');
        $this->denuncia->denunciasAssociadas()->sync($denunciasRelacionadas);
        return view('denuncia.finalizado')->with('denuncia', $this->denuncia);
    }

    private function _setValuesFromCep()
    {
        $informacoesFromCep = ZipCode::find($this->denuncia->cep);
        if ($informacoesFromCep) {
            $informacoesFromCep = $informacoesFromCep->getArray();
            $this->denuncia->cidade = $informacoesFromCep['localidade'];
            $this->denuncia->bairro = $informacoesFromCep['bairro'];
            $this->denuncia->rua = $informacoesFromCep['logradouro'];
            $this->denuncia->uf = $informacoesFromCep['uf'];
        }
    }

    private function _saveFiles($files)
    {
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $size = $file->getSize();
            $name = $file->getClientOriginalName();
            if ($this->_isValidExtension($extension) && $this->_isValidSize($size)) {
                $storagePath = $file->storeAs(
                    'arquivos/' . Carbon::now()->toDateString(), md5($name . rand())
                );
                $this->denuncia->arquivos()->create([
                    'path' => $storagePath,
                    'extensao' => $extension
                ]);
            }
        }
    }

    private function _isValidExtension($extension)
    {
        $validExtensions = [
            'mpeg', 'mp4', 'mp3', 'wmv', 'mpeg',
            '3gpp', 'avi', 'jpeg', 'png', 'pdf', 'doc', 'docx', 'txt', 'jpg'
        ];
        return in_array($extension, $validExtensions);
    }

    private function _isValidSize($size)
    {
        return !($size == 0) && $size < 4999999;
    }

    public function pedirAcompanhamento()
    {
        return view('denuncia.pedido_acompanhamento');
    }

    public function acompanhamento(AcompanhamentoRequest $request)
    {
        $protocolo = $request->input('protocolo');
        $password = $request->input('password');

        $denunciaPesquisada = $this->denuncia->where('protocolo', $protocolo)->get()->first();

        if (! Hash::check( $password, $denunciaPesquisada->password )){
            return redirect()->back()->withErrors(
                ['senha' => 'Número de protocolo ou Senha Incorreta']
            );
        }
        return view('denuncia.tela_cidadao', compact(
            'denunciaPesquisada'
        ));
    }
}
