<?php namespace dredd\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AcompanhamentoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'protocolo' => "required|integer|min:12 |exists:denuncia,protocolo|digits:13",
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => "O campo :attribute é obrigatório",
            'protocolo.required' => "O número de protocolo é obrigatório",
        ];
    }
}
