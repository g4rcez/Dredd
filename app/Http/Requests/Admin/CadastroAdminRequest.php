<?php namespace dredd\Http\Requests\Admin;
use Illuminate\Foundation\Http\FormRequest;

class CadastroAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required|min:3|string|max:64',
            'cpf' => 'required|cpf|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:9|confirmed',
        ];
    }

    public function messages()
    {
        $messages = [
            'login.required' => 'O Login é obrigatório.',
            'login.min' => 'O Login deve possuir no mínimo 3 caracteres.',
            'login.max' => 'O Login deve possuir no máximo 64 caracteres.',
            'login.string' => 'O Login deve ser alfabético.',

            'email.required' => 'O email é obrigatório.',
            'email.email' => 'Deve ser informado um email válido.',
            'email.unique' => 'O email deve ser único dos usuários cadastrados.',
            'email.max' => 'O email deve possuir 255 caracteres no máximo.',

            'password.required' => 'A senha é obrigatória.',
            'password.confirmed' => 'Senha e sua confirmação não conferem.',
            'password.min' => 'São obrigatórios 9 caracteres para a senha(valor mínimo).',

            'cpf.cpf' => 'Valor informado não está no padrão de CPF.',
            'cpf.unique' => 'Verifique se o CPF está correto ou já existe no sistema.',
        ];
        return $messages;
    }
}
