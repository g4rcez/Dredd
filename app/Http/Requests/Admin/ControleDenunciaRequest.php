<?php namespace dredd\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ControleDenunciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo' => 'min:3|max:50|string|unique:tipos_denuncia|required',
            'descricao' => 'min:10|max:1000|string|required',
            'cor' => 'min:4|max:7|string|required',
        ];
    }

    public function messages()
    {
        return [
            'titulo.required' => 'O Título é obrigatório',
            'descricao.required' => 'A descrição é obrigatória',
            'cor.required' => 'A cor é obrigatória',

            'titulo.min' => "O Título deve conter mais do que 3 caracteres e menos do que 50",
            'titulo.max' => "O Título deve conter mais do que 3 caracteres e menos do que 50",

            'descricao.min' => "A descrição deve conter mais do que 10 caracteres e menos do que 1000",
            'descricao.max' => "A descrição deve conter mais do que 10 caracteres e menos do que 1000",

            'cor.min' => "A cor deve seguir o padrão #abcdef",
            'cor.max' => "A cor deve seguir o padrão #abcdef",
        ];
    }
}
