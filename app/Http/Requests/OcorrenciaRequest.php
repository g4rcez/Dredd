<?php namespace dredd\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Request;

class OcorrenciaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $nowHour = date('d/m/Y');
        $horaBefore = date('H:i');
        $recebido = Request::input('data');

        if ($nowHour == $recebido) {
            $hora = [ 'hora' => "required | between:3,5 | date_format:H:i| before:$horaBefore",];
        } else {
            $hora = [ 'hora' => "required | between:3,5 | date_format:H:i",];
        }

        $this->sanitize();

        $rules = [
        'tipos_denuncia_id' => "required",
        'data' => "required | min:8 | max: 10 | before:tomorrow | date_format:d/m/Y",
        'cep' => "required | min:8 | max:9 | cep_format",
        'texto' => "required | min:20 | max:2500",
        'password' => "required | min: 9"
      ];
        return array_merge($rules, $hora);
    }

    public function sanitize()
    {
      $inputs = $this->all();
      // $inputs['denuncia_escrita'] = htmlspecialchars(filter_var($inputs['denuncia_escrita'], FILTER_SANITIZE_STRING), ENT_QUOTES);
      $this->replace($inputs);
    }

    public function messages()
    {
        $today = date('d/m/Y');
        $nowHour = date('d/m/Y H:i');
        $messages = [
          'required' => 'O campo :attribute é obrigatório',
          'tipos_denuncia_id.required' => 'O campo Ocorrência requer pelo menos um item marcado.',
          'data.required' => "O campo Data é obrigatório",
          'hora.required' => "O campo hora é obrigatório",
          'cep.required' => 'O campo :attribute é obrigatório',
          'texto.required' => "O campo descrição é obrigatório",

          'data.before' => "Data deve ser anterior a $today",
          'data.min' => "Data contém carácteres a menos do que o padrão DD/MM/AAAA",
          'data.max' => "Data contém carácteres a mais do que o padrão DD/MM/AAAA",
          'hora.before' => "A Hora deve ser anterior a $nowHour",
          'hora.min' => "Hora contém carácteres a menos do que o padrão HH:MM",
          'hora.max' => "Hora contém carácteres a mais do que o padrão HH:MM",
          'texto.min' => "A denúncia escrita deve conter no mínimo, 20 caracteres",
          'texto.max' => "A denúncia escrita deve conter no máximo, 2500 caracteres",

          'cep.cep_format' => "O campo :attribute está fora do padrão 00000-000",
          'cep.max' => "Cep contém números a mais do que o padrão 00000-000",
          'cep.min' => "Cep contém números a menos do que o padrão 00000-000",

          'password.min' => 'A senha requer 9 caracteres ou mais',
        ];
        return $messages;
    }
}
