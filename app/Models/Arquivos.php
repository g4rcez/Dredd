<?php
namespace dredd;
use Illuminate\Database\Eloquent\Model;

class Arquivos extends Model
{
    protected $table = 'arquivos';
    protected $fillable = [
        'path', 'extensao'
    ];

    public $timestamps = false;

    public function denuncia()
    {
        return $this->belongsTo('app\Models\Denuncia', 'denuncia_id');
    }
}
