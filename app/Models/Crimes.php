<?php namespace dredd\Models;
use Illuminate\Database\Eloquent\Model;

class Crimes extends Model
{
    protected $table = "crimes";

    protected $fillable = [
        'titulo', 'descricao', 'cor', 'gravidade'
    ];

    public function tiposDenuncia()
    {
        return $this->belongsToMany(Denuncia::class, 'denuncias_associadas', 'denuncia_id', 'tipos_denuncia_id');
    }

    public function denuncia()
    {
        return $this->belongsTo('dredd\Models\Crimes');
    }
}
