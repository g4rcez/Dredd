<?php namespace dredd\Models;

use Illuminate\Database\Eloquent\Model;

class Denuncia extends Model
{
    protected $table = 'denuncia';
    protected $fillable = [
        'data_hora', 'cep', 'cidade', 'bairro',
        'rua', 'uf' , 'texto', 'password',
    ];

    protected $dates = [ 'data_hora' ];

    public function setProtocolo()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $data = (date('Ymd'));
        $dataSegundos = (date('G') * 3600) + (date('i') * 60) + (date('s'));
        return "$data"."$dataSegundos";
    }

    public function denunciasAssociadas()
    {
        return $this->belongsToMany(
            Crimes::class, 'crimes_marcados',
            'denuncia_id', 'crimes_id'
        );
    }

    public function arquivos()
    {
        return $this->hasMany('dredd\Arquivos', 'denuncia_id', 'id');
    }
}
