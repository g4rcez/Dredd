<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TodasTabelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
<<<<<<< HEAD
        Schema::create('denuncia_cidadao', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('numero_protocolo', 13)->nullable();
            $table->dateTime('data_hora')->nullable();
            $table->string('cep', 9)->nullable();
            $table->string('cidade', 45)->nullable();
            $table->string('bairro', 45)->nullable();
            $table->string('rua', 45)->nullable();
            $table->string('uf', 3)->nullable();
            $table->string('password', 64)->nullable();
            $table->string('denuncia_escrita', 2500)->nullable();
            $table->integer('denuncia_original')->unsigned()->nullable();

            $table->index('denuncia_original', 'fk_denuncia_cidadao_denuncia_cidadao1_idx');

            $table->foreign('denuncia_original')
                ->references('id')->on('denuncia_cidadao');

            $table->timestamps();
        });

        Schema::create('status_denuncia', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('id');
            $table->string('nome_status', 45)->nullable();

            $table->primary('id');

            $table->timestamps();
        });

        Schema::create('acompanhamento_denuncia', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('id');
            $table->integer('denuncia_cidadao_id')->unsigned();
            $table->integer('status_denuncia_id')->unsigned();
            $table->string('data_atualizacao', 45)->nullable();

            $table->primary('id');

            $table->index('denuncia_cidadao_id', 'fk_acompanhamento_denuncia_denuncia_cidadao_idx');
            $table->index('status_denuncia_id', 'fk_acompanhamento_denuncia_status_denuncia1_idx');

            $table->foreign('denuncia_cidadao_id')
                ->references('id')->on('denuncia_cidadao');

            $table->foreign('status_denuncia_id')
                ->references('id')->on('status_denuncia');

            $table->timestamps();
        });

        Schema::create('tipos_denuncia', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('titulo', 50)->nullable();
            $table->string('descricao', 500)->nullable();
            $table->string('cor', 45)->nullable();

            $table->timestamps();
        });

        Schema::create('agente_modificador', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->dateTime('created_at')->nullable();
            $table->integer('denuncia_cidadao_id')->unsigned();

            $table->index('denuncia_cidadao_id', 'fk_agente_modificador_denuncia_cidadao1_idx');

            $table->foreign('denuncia_cidadao_id')
                ->references('id')->on('denuncia_cidadao');

            $table->timestamps();
        });

        Schema::create('denuncias_associadas', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('tipos_denuncia_id')->unsigned();
            $table->integer('denuncia_cidadao_id')->unsigned();

            $table->primary('tipos_denuncia_id', ' denuncia_cidadao_id');

            $table->index('denuncia_cidadao_id', 'fk_tipos_denuncia_has_denuncia_cidadao_denuncia_cidadao1_idx');
            $table->index('tipos_denuncia_id', 'fk_tipos_denuncia_has_denuncia_cidadao_tipos_denuncia1_idx');

            $table->foreign('tipos_denuncia_id')
                ->references('id')->on('tipos_denuncia');

            $table->foreign('denuncia_cidadao_id')
                ->references('id')->on('denuncia_cidadao');

            $table->timestamps();
        });
=======
        if (!Schema::hasTable('denuncia_cidade')) {
            Schema::create('denuncia_cidadao', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('numero_protocolo', 13)->nullable();
                $table->dateTime('data_hora')->nullable();
                $table->string('cep', 9)->nullable();
                $table->string('cidade', 45)->nullable();
                $table->string('bairro', 45)->nullable();
                $table->string('rua', 45)->nullable();
                $table->string('uf', 3)->nullable();
                $table->string('denuncia_escrita', 2500)->nullable();
                $table->integer('denuncia_original')->unsigned()->nullable();

                $table->index('denuncia_original', 'fk_denuncia_cidadao_denuncia_cidadao1_idx');

                $table->foreign('denuncia_original')
                    ->references('id')->on('denuncia_cidadao');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('status_denuncia')) {
            Schema::create('status_denuncia', function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->integer('id');
                $table->string('nome_status', 45)->nullable();

                $table->primary('id');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('status_denuncia')) {
            Schema::create('acompanhamento_denuncia', function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->integer('id');
                $table->integer('denuncia_cidadao_id')->unsigned();
                $table->integer('status_denuncia_id')->unsigned();
                $table->string('data_atualizacao', 45)->nullable();

                $table->primary('id');

                $table->index('denuncia_cidadao_id', 'fk_acompanhamento_denuncia_denuncia_cidadao_idx');
                $table->index('status_denuncia_id', 'fk_acompanhamento_denuncia_status_denuncia1_idx');

                $table->foreign('denuncia_cidadao_id')
                    ->references('id')->on('denuncia_cidadao');

                $table->foreign('status_denuncia_id')
                    ->references('id')->on('status_denuncia');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('tipos_denuncia')) {
            Schema::create('tipos_denuncia', function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->increments('id');
                $table->string('titulo', 50)->nullable();
                $table->string('descricao', 500)->nullable();
                $table->string('cor', 45)->nullable();

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('agente_modificador')) {
            Schema::create('agente_modificador', function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->increments('id');
                $table->integer('denuncia_cidadao_id')->unsigned();

                $table->index('denuncia_cidadao_id', 'fk_agente_modificador_denuncia_cidadao1_idx');

                $table->foreign('denuncia_cidadao_id')
                    ->references('id')->on('denuncia_cidadao');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('denuncias_associadas')) {
            Schema::create('denuncias_associadas', function (Blueprint $table) {
                $table->engine = 'InnoDB';

                $table->integer('tipos_denuncia_id')->unsigned();
                $table->integer('denuncia_cidadao_id')->unsigned();

                $table->primary('tipos_denuncia_id', ' denuncia_cidadao_id');

                $table->index('denuncia_cidadao_id', 'fk_tipos_denuncia_has_denuncia_cidadao_denuncia_cidadao1_idx');
                $table->index('tipos_denuncia_id', 'fk_tipos_denuncia_has_denuncia_cidadao_tipos_denuncia1_idx');

                $table->foreign('tipos_denuncia_id')
                    ->references('id')->on('tipos_denuncia');

                $table->foreign('denuncia_cidadao_id')
                    ->references('id')->on('denuncia_cidadao');

                $table->timestamps();
            });
        }
>>>>>>> insertFiles
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('denuncia_cidadao');
        Schema::dropIfExists('status_denuncia');
        Schema::dropIfExists('acompanhamento_denuncia');
        Schema::dropIfExists('tipos_denuncia');
        Schema::dropIfExists('agente_modificador');
        Schema::dropIfExists('denuncias_associadas');
    }
}
