<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabelaArquivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('arquivos')) {
            Schema::create('arquivos', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('extensao', 4)->nullable();
                $table->string('path', 100)->nullable();
                $table->integer('denuncia_cidadao_id')->unsigned();

                $table->index('denuncia_cidadao_id', 'fk_arquivos_denuncia_cidadao1_idx');

                $table->foreign('denuncia_cidadao_id')
                    ->references('id')->on('denuncia_cidadao');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('arquivos');
    }
}
