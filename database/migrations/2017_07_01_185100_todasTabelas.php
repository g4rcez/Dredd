<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('denuncia', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->increments('id');
		    $table->string('protocolo', 13)->nullable();
		    $table->dateTime('data_hora')->nullable();
		    $table->string('cep', 9)->nullable();
		    $table->string('cidade', 45)->nullable();
		    $table->string('bairro', 45)->nullable();
		    $table->string('rua', 45)->nullable();
		    $table->string('uf', 3)->nullable();
		    $table->string('texto', 2500)->nullable();
		    $table->dateTime('created_at')->nullable();
		    $table->dateTime('updated_at')->nullable();
		    $table->string('password', 200)->nullable();
		    $table->integer('denuncia_original')->unsigned()->nullable();

		    $table->index('denuncia_original','fk_denuncia_denuncia1_idx');

		    $table->foreign('denuncia_original')
		        ->references('id')->on('denuncia');

		    $table->timestamps();

		});

		Schema::create('status_denuncia', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->integer('id');
		    $table->string('nome_status', 45)->nullable();
		    $table->string('created_at', 45)->nullable();
		    $table->string('updated_at', 45)->nullable();

		    $table->primary('id');

		    $table->timestamps();

		});

		Schema::create('acompanhamento_denuncia', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->integer('id');
		    $table->integer('status_denuncia_id')->unsigned();
		    $table->string('data_atualizacao', 45)->nullable();
		    $table->dateTime('created_at')->nullable();
		    $table->dateTime('updated_at')->nullable();
		    $table->integer('denuncia_id')->unsigned();

		    $table->primary('id');

		    $table->index('status_denuncia_id','fk_acompanhamento_denuncia_status_denuncia1_idx');
		    $table->index('denuncia_id','fk_acompanhamento_denuncia_denuncia1_idx');

		    $table->foreign('status_denuncia_id')
		        ->references('id')->on('status_denuncia');

		    $table->foreign('denuncia_id')
		        ->references('id')->on('denuncia');

		    $table->timestamps();

		});

		Schema::create('tipos_denuncia', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->increments('id');
		    $table->string('titulo', 50)->nullable();
		    $table->string('descricao', 500)->nullable();
		    $table->string('cor', 45)->nullable();
		    $table->string('created_at', 45)->nullable();
		    $table->string('updated_at', 45)->nullable();

		    $table->timestamps();

		});

		Schema::create('agente_modificador', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->increments('id');
		    $table->dateTime('created_at')->nullable();
		    $table->dateTime('updated_at')->nullable();
		    $table->integer('denuncia_id')->unsigned();

		    $table->index('denuncia_id','fk_agente_modificador_denuncia1_idx');

		    $table->foreign('denuncia_id')
		        ->references('id')->on('denuncia');

		    $table->timestamps();

		});

		Schema::create('permissoes', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->increments('id');
		    $table->string('nome_grupo', 45)->nullable();
		    $table->string('tag', 5)->nullable();

		    $table->timestamps();

		});

		Schema::create('user', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->increments('id');
		    $table->string('login', 64)->nullable();
		    $table->string('email', 100)->nullable();
		    $table->string('password', 20)->nullable();
		    $table->string('cpf', 15)->nullable();
		    $table->integer('es_id');

		    $table->timestamps();

		});

		Schema::create('denuncias_associadas', function(Blueprint $table) {
		    $table->engine = 'InnoDB';

		    $table->integer('tipos_denuncia_id')->unsigned();
		    $table->integer('denuncia_id')->unsigned();

		    $table->primary('tipos_denuncia_id', ' denuncia_id');

		    $table->index('denuncia_id','fk_tipos_denuncia_has_denuncia_denuncia1_idx');
		    $table->index('tipos_denuncia_id','fk_tipos_denuncia_has_denuncia_tipos_denuncia1_idx');

		    $table->foreign('tipos_denuncia_id')
		        ->references('id')->on('tipos_denuncia');

		    $table->foreign('denuncia_id')
		        ->references('id')->on('denuncia');

		    $table->timestamps();

		});


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('denuncia');
		Schema::drop('status_denuncia');
		Schema::drop('acompanhamento_denuncia');
		Schema::drop('tipos_denuncia');
		Schema::drop('agente_modificador');
		Schema::drop('permissoes');
		Schema::drop('user');
		Schema::drop('denuncias_associadas');

    }
}
