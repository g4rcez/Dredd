<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        factory('dredd\Models\Crimes', 10)->create();
    }
}
