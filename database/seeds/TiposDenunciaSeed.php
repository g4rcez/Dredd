<?php

use Illuminate\Database\Seeder;

class TiposDenunciaSeed extends Seeder
{
    public function run()
    {
        DB::table('lista_tipos_denuncias')->insert([
             'titulo' => str_random(10),
             'descricao' => str_random(30),
             'cor' => '#f20808'
         ]);
    }
}
