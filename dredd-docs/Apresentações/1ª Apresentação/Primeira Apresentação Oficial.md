<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- $theme: gaia -->
<!-- template: invert-->
<!-- footer: Equipe de Desenvolvimento Dredd -->
<!-- prerender: true-->

Sistema de Web Denúncia
===
#### O Sistema de Apoio ao ==**Giforseg**==

---
<!-- *template: gaia-->
# Apresentação do Sistema
#### Uma visão do que está por vir
---

### Combate ao Crime
O Sistema visa auxiliar o ==**GIFORSEG**== no combate aos crimes ocorridos na cidade de Juiz de Fora.

Sendo alternativo ao atual sistema de **Disque Denúncia 181**, este novo sistema irá garantir uma denúncia anônima, sem a necessidade de locomoção a um Departamento da Polícia. Além do apoio estátistico ao ==**GIFORSEG**==.

A seguir, você pode conferir uma prévia da tela inicial do sistema, apelidado de Dredd.

---
![bg original](telaInicial.png)

---
![bg original](menuVisible.png)

---
# Lembrete :pushpin:
==**Lembrando apenas que é uma versão de apresentação e está sujeita a mudança de acordo com os comentários feitos**==

> Edições visuais e adição da identidade visual do projeto ==**GIFORSEG**== são bons pontos para serem comentados.

---
<!-- *template: gaia-->
# Funcionalidades do Sistema
#### Discussão sobre como deverá ser seu funcionamento
---

# Visão do Cidadão :family:
O Cidadão é a peça chave para que o sistema seja  bem utilizado pelo ==**GIFORSEG**==. É ele quem irá alimentar o sistema com informações de crimes, para que os administradores do sistema sejam informados e as denúncias, devidamente encaminhadas.

O Cidadão também poderá acompanhar o andamento das investigações ou outras operações realizadas. Mas para isso, um administrador deverá atualizar a denúncia do Cidadão.

---
# Pergunta da equipe :question:
1. Poderão ser feitas todos os tipos de denúncia através deste sistema?
2. O registro da denúncia será formalizado como documento?
3. As atualizações feitas pelo ==**GIFORSEG**== irão gerar um novo número de protocolo para o Cidadão, ou permanecerá sempre o protocolo inicial?
4. O Cidadão poderá atualizar sua denúncia após a mesma sendo feita e enviada?


---
<!-- *template: gaia-->
# Tópico 2.0
#### Subtítulo do tópico
---

# Cabeçalho h1
## Cabeçalho h2
### Cabeçalho h3
#### Cabeçalho h4
##### Cabeçalho h5
###### Cabeçalho h6

---
<!-- *template: gaia-->
# Tópico 3.0
#### Subtítulo do tópico
---

# ImagemBG(sem efeitos) + Links
![bg original GitPageBackground](http://orig08.deviantart.net/9e94/f/2013/303/d/e/github___arc___wallpaper_by_cracksoldier-d6se2bp.png)
[GitHub da Uname](http://github.com/unamecorporation)

---
# ImagemBG(com efeitos) + Links
![bg GitPageBackground](http://orig08.deviantart.net/9e94/f/2013/303/d/e/github___arc___wallpaper_by_cracksoldier-d6se2bp.png)
[GitHub da Uname](http://github.com/unamecorporation)

---
# Imagem no Slide
![43% GitPageBackground](http://orig08.deviantart.net/9e94/f/2013/303/d/e/github___arc___wallpaper_by_cracksoldier-d6se2bp.png)

---
<!-- *template: gaia-->
# Tópico 4.0
#### Subtítulo do tópico
---

# Expressões Matemáticas
$$I_{xx}=\int\int_Ry^2f(x,y)\cdot{}dydx$$
$$x=\forall x \in X, \quad \exists y \leq \epsilon
$$

$$\alpha\beta\gamma\pi\Pi\phi\varphi\mu\Phi$$
- [Sintaxe Katex](https://en.wikibooks.org/wiki/LaTeX/Mathematics)
- [Sintaxe Latex](https://wch.github.io/latexsheet/latexsheet.pdf)