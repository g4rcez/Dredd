###### Segunda, 08. maio 2017 23:03 
# **Dredd**
### O Sistema de Ouvidoria 

> Dredd é um sistema para ouvidoria em empresas e órgãos de pesquisa. O sistema deve ser capaz de armazenar as reclamações feitas pelos usuários e informar em um painel administrativo, com gráficos e outros tipos de relatório, tudo o que foi informado pelos usuários

### Versão v1.0.0 - GIFORSEG

Na primeira versão de Dredd, seu propósito comercial é agir como um "Disque Denúncia em versão Web". O programa será desenvolvido para a Polícia Civil Local. É esperado auxiliar a polícia com este sistema, gerando relatórios de acordo com as ocorrências informadas pelos usuários do sistema(cidadãos).

### Caso de Uso
> Dredd é o sistema gerenciador de ocorrências feitas pelos habitantes da cidade em que está hospedado. Polícia será o identificador de todos os agentes que representam o conjunto de administradores do sistema. Cidadão é o conjunto identificador de todos os cidadãos que podem fazer uma ocorrência para o sistema. A seguir, uma ilustração de como funcionará o processo de fazer uma ocorrência, investigação, preenchimento de dados e conclusão da mesma.

- Cidadão irá acessar o site que redirecionará para o sistema Dredd. Neste site, o cidadão será recebido com uma página explicando seus passos para fazer uma ocorrência.
- Após ler e concordar com os 'Termos de Uso', o Cidadão será redirecionado para uma página onde existem três fases simples para realizar sua denúncia:
	- **1ª fase:** Uma tela indicando a primeira fase, onde haverá um número "O" de tipos de ocorrências possíveis para referenciar qual crime está sendo denunciado. 
	- **2ª fase:** Uma tela indicando a segunda fase, onde haverá locais para o Cidadão dar informações referentes ao local, data e hora de onde ocorreu o crime que está sendo denunciado. Para data e hora, é esperado um valor aproximado.
	- **3ª fase:** Uma tela indicando a terceira fase, onde haverá apenas 2 campos para preencher com informações. Sendo o primeiro campo pessoal, permitindo o Cidadão escrever até 2000 caracteres sobre o crime ocorrido. O segundo campo é opcional, onde o Cidadão poderá enviar um arquivo de áudio, vídeo, foto ou documento, fornecendo provas para a Polícia.
- Após as três fases ocorridas, o Cidadão será redirecionado para uma tela de confirmação da ocorrência feita. Cidadão deverá conferir se todos os dados realmente estão de acordo com o ocorrido, caso estejam, basta submeter a ocorrência para Dredd.
- Na tela seguinte, Cidadão receberá um número de protocolo e um número de confirmação, estes sendo indicados a não serem compartilhados, garantido o anonimato de Cidadão.
- Os dois números recebidos por Cidadão serão sua forma de acompanhar o andamento de sua denúncia. Podendo ser acessada sempre que desejar para ser feito o acompanhamento.
- Conforme a Polícia investiga, um agente especializado será encarregado de inserir informações sobre a ocorrência de Cidadão, até que a investigação seja encerrada, assim como o caso.
- Após dado por encerrado o caso, Cidadão recebe em sua tela de acompanhamento todas as informações passadas pela Polícia, podendo baixar arquivos, caso existam, e verificar mais informações.

---

### Escopo do Sistema:
- Dredd será um intermediário entre Cidadão e Polícia. Registrando todas as ocorrências feitas pelo Cidadão, as armazenando em seu banco de dados e posteriormente, retornando as ocorrências a Polícia.
- Dredd deverá armazenar as ocorrências por ordem de entrada e informadas a Polícia em formato de Fila (FIFO). Conforme as ocorrências forem resolvidas, a fila irá andando.
- Dredd deverá ser capaz de informar ao Cidadão toda a trajetória da ocorrência feita, conforme as informações da mesma forem sendo adicionadas pela Polícia.
- Por tempo indeterminado, a denúncia feita pelo Cidadão será mantida no banco de dados, para que o mesmo possa utilizar como prova em casos jurídicos.
- Garantir o anonimato da denúncia feita pelo Cidadão, contanto que este siga meticulosamente os requisitos indicados nos 'Termos de Uso'

### Não-Escopo do Sistema:
- Dredd não será capaz de identificar a origem de quaisquer ocorrências feitas pelo Cidadão.
- Dredd não irá identificar autores de crimes denunciados pelo Cidadão, mesmo que nomes sejam citados.
- Dredd não é capaz de tomar decisões por si só, apenas organizar todas as informações que o Cidadão submete a Polícia.
- Dredd não será capaz de congelar bens cibernéticos de pessoas investigadas.

#### Versões Futuras
- Identificação, em tempo real, de crimes ocorridos, de acordo com informações postadas em Facebook, Twitter e Jornais Locais(neste caso, Tribuna de Minas).
- Filtro para correção de escrita submetida pelo Cidadão, corrigindo erros ortográficos e omitindo nomes que talvez poderiam anunciar a real identidade de Cidadão
- Identificação de ocorrências através de formulários pré moldados, para auxiliar pessoas com dificuldades de escrita a fazerem sua denúncia (Acessibilidade do sistema).
- Prever denúncias trotes através do texto.

---

### Requisitos Funcionais
- Submeter ocorrências feitas por Cidadão e formatar as informações obtidas através de gráficos e documentos para futuras análises feitas pela Polícia
- Garantir o anonimo de Cidadão, de forma que nem mesmo a Polícia possa identificar o mesmo. **Levando em conta que Cidadão obedeceu todos os 'Termos de Uso'**
- Trazer informações oportunas para Polícia, de acordo com as informações dadas pelo Cidadão.
- Alertar a Polícia sobre ocorrências em atraso, melhorar o *feedback* sobre crimes ocorridos em dada hora, local e datas específicas.

### Requisitos não Funcionais
- Garantir a segurança do sistema, evitando ataques feitos na aplicação, e mitigando vulnerabilidades do servidor, por não abrir brechas de segurança.
- Garantir que o Cidadão não irá revelar sua identidade.