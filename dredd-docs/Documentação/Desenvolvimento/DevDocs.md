# Padrões do Projeto Dredd
> Definição de todos os tipos de classes, objetos, variáveis, controllers, requests, models e quaisquer elementos que possam ser utilizados no projeto Dredd...

## Padrões gerais de nome
- Todo nome deve ser significativo
- Utilizar CamelCase para variáveis, utilize snake_case somente no banco e em pastas do projeto
- Utilizar os termos em inglês para simplificar alguns nomes, como por exemplo *getNome()* e *setNome('Javalino')*

## Padrões para Classes e Controllers
- Obedecer a **Padrões gerais de nome** em todos os requisitos.
- Todo nome de arquivo Controller deve obedecer a regra: **NomeController**
- Ao instanciar classes, deve se dar o nome **objNomeObjeto**

## Padrões para Variáveis
- As variáveis devem conter um prefixo identificando qual seu tipo. Por exemplo:
	- Inteiro: intIdProtocol
	- String: strTexto
	- Float e Double: realDinheiro
	- Booleano: boolNumeroPrimo
	- Caractere: charOpcao