<!-- $size: A4 -portrait -->
<!-- page_number: true -->
<!-- $theme: default -->
<!-- footer: Equipe de Desenvolvimento IFJF-->
<!-- prerender: true-->
<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat|Quicksand');
    body{
    	height: 100%;
        width:90%;
        margin:auto;
    }
    p{
    	text-align: justify;
        text-indent: 2em;
    }
    p, ul, li{
    	font-family: Quicksand, sans-serif;
        color:#121212;
    }
    h1, h2, h3, h4, h5, h6{
    	font-family: Montserrat, sans-serif;
    }
</style>


# Dredd
## Sistema de Apoio ao **GIFORSEG**
###### Este documento possui o intuito de auxiliar os agentes usuários do sistema. Neste manual você encontrará uma escrita mais amigável para grandes leituras, facilitando a assimilação de conteúdo quanto ao uso do sistema
---

# <center>Agradecimentos</center>
Mussum Ipsum, cacilds vidis litro abertis. Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Cevadis im ampola pa arma uma pindureta. Sapien in monti palavris qui num significa nadis i pareci latim. Tá deprimidis, eu conheço uma cachacis que pode alegrar sua vidis.

---

# <center>Sumário</center>
1. Introdução <small>**NN**</small>
2. Especificações de Segurança <small>**NN**</small>
 2.1. Políticas de Senha
 2.2. 
3. Gerenciamento de Denúncias
4. Gerenciamento de Usuários
5. Gerenciamento de Permissões dos Usuários
6. Geração de Relatório
7. Sugestões e Reclamações
---

# <center>1. Introdução</center>
Este é um sistema de apoio estratégico para o Gabinete Interinstitucional das Forças de Segurança (**GIFORSEG**), com o intuito de gerenciar as denuncias feitas e auxiliar os agentes a identificar problemas, assim como resolve-los, apresentando relatórios de acordo com a necessidade.

Com este sistema, os agentes poderão ter um respaldo de todo o perfil de crimes da cidade, sabendo o local, data e hora de registro e incidentes, tipos de crimes e filtros personalizados.

---

# 2. Especificações de Segurança
Para que tudo possa funcionar da melhor forma possível e que sejam evitados os indesejaveis ataques de crackers, este sistema exige a colaboração de seus usuários.

As políticas do sistema estão descritas por subtópicos, facilitando a leitura e o entendimento de cada seção do documento.

---
# 2.1 Políticas de Senha :lock:
Para que não hajam ataques bem sucedidos contra as contas cadastradas no sistema, deverá haver uma forte política quanto a definição de senhas. Os seguintes critérios deverão ser obedecidos para que uma senha possa ser validada.

- Toda senha deverá possuir **8 caracteres no mínimo**.
- Toda senha deverá conter **letras, números e simbolos especiais**.
- Toda senha deverá conter **letras maisculas e minusculas**
- **É aconselhavel não utilizar senhas que contenham datas de nascimento e nomes de parentes próximos, animais de estimação e afins.**

Exemplo de uma senha forte com 8 caracteres: **#Dr3dd!#**

---

# 2.2 Item


