;(function ($) {
    'use strict';
    $(document).ready(function () {
        var message = ' arquivo.';
        $('#fileupload').on('change', function () {
            $('#selectedFiles').empty();
            $('#refreshFiles').empty();
            var fileArray = this.files;
            var index = 0;
            var retorno = true;
            $.each(fileArray, function (event, file) {
                retorno *= checkFileExtension(file);
                index++;
            });
            if (!retorno) {
                $('#refreshFiles').append('É aconselhavel você carregar apenas os arquivos que serão salvos.');
            } else {
                $('#refreshFiles').append('');
            }
            if (index > 1) {
                message = ' arquivos.';
            }
            $('#numberFiles').text(index + message);
        });
    });

    function checkFileExtension(file) {
        var size = file.size;
        var name = file.name;
        var fileName, fileExtension, formatsAccepted;
        var retorno = false;
        formatsAccepted =
            ['audio/mpeg',
                'video/mp4',
                'video/x-ms-wmv',
                'video/mpeg',
                'audio/mpeg3',
                'video/3gpp',
                'video/avi',
                'image/jpeg',
                'image/png',
                'image/gif',
                'application/pdf',
                'application/msword',
                'text/plain'];
        if (!formatsAccepted.includes(file.type)) {
            $('#selectedFiles').append("<p><span class='button label label-danger'>" + name + "</span> <span class='button label label-warning'>Tipo não suportado.</span></p>");
        } else if (0 == size) {
            $('#selectedFiles').append("<p><span class='button label label-danger'>" + name + "</span> <span class='button label label-warning'>Tamanho insuficiente.</span></p>");
        }
        else if (size > 4999999) {
            $('#selectedFiles').append("<p><span class='button label label-danger'>" + name + "</span> <span class='button label label-danger'>Tamanho superior ao suportado.</span></p>");
        }
        else {
            $('#selectedFiles').append("<p><span class='button label label-success'>" + name + "</span></p>");
            retorno = true;
        }
        return retorno;
    }
})(window.jQuery);