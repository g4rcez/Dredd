<html>
<head>
  <meta charset="utf-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="keywords" content="Sistema Gordon" />
   <meta name="description" content="" />
   <title>Dredd GIFORSEG - Login</title>
   <meta name="apple-mobile-web-app-capable" content="yes">
   <meta name="apple-mobile-web-app-status-bar-style"content="black-translucent">
   <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
   <link href="{!! asset('/css/font-awesome.min.css') !!}" rel='stylesheet' type='text/css' />
   <link href="{!! asset('/admin/dist/css/fonts.css') !!}" rel='stylesheet' type='text/css' />
   <link href="{!! asset('/admin/dist/css/login.css') !!}" rel='stylesheet' type='text/css' />
</head>
<body>
  <div class="wrapper fadeInDown">
    <div id="formContent">
      <!-- Tabs Titles -->
      <h2 class="active"> Bem Vindo ao Sistema do <strong>GIFORSEG</strong> </h2><br /><br />
      <!-- Icon -->
      <div class="fadeIn first">
        <i class="fa fa-user-secret" aria-hidden="true" style="font-size:8em;"></i>
      </div>

      <!-- Login Form -->
      {!! Form::open(['url' => '/dredd/_giforseg/login']) !!}
        <input type="text" id="login" class="fadeIn third" name="login" placeholder="Digite seu Login...">
        <input type="password" id="password" class="fadeIn third" name="password" placeholder="Digite sua Senha...">
        <br />
        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} id='remember'> <label for='remember'>Manter Logado?</label><br />
        <input type="submit" class="fadeIn fourth" value="Login">
      {!! Form::close() !!}
      <!-- Remind Passowrd -->
      <div id="formFooter">
        <a class="underlineHover" href="#">Esqueceu sua senha?</a>
      </div>
    </div>
  </div>
</body>
</html>
