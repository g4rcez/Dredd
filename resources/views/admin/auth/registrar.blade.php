@section('title','- Registro de Usuário')
@extends('admin.layout')
@section('content')

    <div class='content-wrapper'>
        <section class="content-header">
            <h1>
                Painel de Controle<small> Menu principal</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!!url('/dredd/_giforseg/dashboard')!!}"><i class="fa fa-dashboard"></i> Painel Principal</a></li>
            </ol>
        </section>

        <section class='content'>
    <div class="container">
        <div class="row">
            <div class="col-sm-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Registro de Usuários</h3>
                    </div>
                    <div class="panel-body">
                        @include('/admin/sections/cadastro_agente')
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </section>

@endsection