<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box bg-{!! $background !!}">
    <span class="info-box-icon"><i class="fa fa-line-chart"></i></span>
    <div class="info-box-content">
      <span class="info-box-text">{!! $titulo !!}</span>
      <span class="info-box-number">{!! $estatistica !!}</span>
      <div class="progress">
        <div class="progress-bar" style="width: {!! $percent !!}%"></div>
      </div>
         <span class="progress-description">
           {!! $desc !!} <i class="fa fa-level-up" aria-hidden="true"></i>
         </span>
      </div>
  </div>
</div>
