{{-- <!-- Date Picker --> --}}
<link rel="stylesheet" href="{!!asset('/admin/plugins/datepicker/datepicker3.css')!!}">
{{-- <!-- daterangepicker --> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{!!asset('/admin/plugins/daterangepicker/daterangepicker.js')!!}"></script>
<script>
    $(document).ready(function () {
    $('#calendario-input').datepicker({
    format: "dd/mm/yyyy",
    language: "pt-BR"
    });
    });
</script>
<div class='box box-solid bg-blue-gradient'>
  <div class='box-header'>
    <i class='fa fa-calendar'></i>
    <h3 class='box-title'>Calendário</h3>
    <!-- tools box -->
    <div class='pull-right box-tools'>
     <!-- button with a dropdown -->
     <button type='button' class='btn btn-info btn-xs' data-widget='collapse'><i class='fa fa-minus'></i>
     </button>
     <button type='button' class='btn btn-info btn-xs' data-widget='remove'><i class='fa fa-times'></i>
     </button>
    </div>
    <!-- /. tools -->
  </div>
  <!-- /.box-header -->
  <div class='box-body no-padding'>
    <p>
      <label for="calendario-input">&nbsp;&nbsp;&nbsp;&nbsp;Calendário</label>&nbsp;
      <input type="text" id="calendario-input" class="form-control data-widget" placeholder="{{ date("d/m/Y") }}" />
    </p>
  </div>
</div>
