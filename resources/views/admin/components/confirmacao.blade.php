<div class="row">
<div class="conteiner-fluid">
  <div class="classe_ocorrencia_final">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="spaces"></div>
      <label for="inputPassword" >Tipos da ocorrência:</label>
        <input class="form-control" id="disabledInput" type="text" placeholder="{!! $protocolo !!}" disabled>
    </div>
  </div>
</div>

<div class="conteiner-fluid">
  <div class="classe_ocorrencia_final">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="spaces"></div>
      <label for="inputPassword">Local da Ocorrência:</label>
      <textarea class="form-control" id="disabledInput" type="text" placeholder="CEP:{!! $cep !!} | Rua: {!! $addr !!}" disabled></textarea>
    </div>
  </div>
</div>
</div>

<div class="row">
<div class="conteiner-fluid">
<div class="classe_ocorrencia_final">
 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
   <div class="spaces"></div>
   <label for="inputPassword" >Data e Hora da Ocorrência:</label>
     <input class="form-control" id="disabledInput" type="text" placeholder="Data: {!! $data !!} | Hora: {!! $hora !!}" disabled>
 </div>
</div>
</div>

<div class="conteiner-fluid">
<div class="classe_ocorrencia_final">
 <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
   <div class="spaces"></div>
   <label for="inputPassword">Descrição da Ocorrência:</label>
   <textarea class="form-control" id="disabledInput" type="text" placeholder="{!! $describe !!}" disabled></textarea>
 </div>
</div>
</div>
</div>
