<div class="row">
  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Gráfico de Relatório: Tipos de Ocorrência</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body text-center">
        <p>Cores na legenda, de acordo com o cadastro de cada ocorrência</p>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-grey" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="height: 89%">
            <span class="sr-only">89%</span>
          </div>
        </div>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-navy" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="height: 10%">
            <span class="sr-only">10%</span>
          </div>
        </div>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="height: 40%">
            <span class="sr-only">40%</span>
          </div>
        </div>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-aqua" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="height: 20%">
            <span class="sr-only">20%</span>
          </div>
        </div>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-yellow" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="height: 60%">
            <span class="sr-only">60%</span>
          </div>
        </div>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="height: 80%">
            <span class="sr-only">80%</span>
          </div>
        </div>
        <div class="progress vertical">
          <div class="progress-bar progress-bar-orange" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="height: 100%">
            <span class="sr-only">100%</span>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col (right) -->
</div>
