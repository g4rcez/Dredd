@php
  $acentos = array("á", "é", "í", "ó", "ú", "ã", "ê", "ì", "ô", "û","â", "è", "î", "õ", "ũ", "à", "è", "î", "ò", "ù");
  $vogais = array('a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u');
  $id = str_replace($acentos, $vogais, trim(strtolower($tipoOcorrencia->titulo)));
  $id = str_replace(" ", "", $id);
@endphp
<div id="{!! $id !!}" class="modal fade modal-ocorrencia" role="dialog">
  <div class="ocorrencia-content">
    <div class="modal-content">
      <div class="ocorrencia-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="ocorrencia-title">{!! $tipoOcorrencia->titulo !!}</h4>
      </div>
      <div class="ocorrencia-body">
        <p>{!! $tipoOcorrencia->descricao !!}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
