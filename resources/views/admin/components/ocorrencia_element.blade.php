@php date_default_timezone_set('America/Sao_Paulo'); @endphp
<div class="ocorrencia-visualizar">
  <h3>
    Protocolo: {{ $denuncia['numero_protocolo'] }} <small>Dia de Registro: {{ $denuncia['created_at'] }}</small>
    <a class="button button-link pull-right" data-toggle="collapse" data-target="#{{ $denuncia['numero_protocolo'] }}">
      Ver Mais <i class="fa fa-plus-square-o" aria-hidden="true"></i>
    </a>
  </h3>
    <div id="{{ $denuncia['numero_protocolo'] }}" class="collapse">
    <h4>Tipos de Ocorrência Marcados</h4>
    <ul class="fa-ul">
      @foreach ($denuncia->denunciasAssociadas as $ocorrencia)
        <li class="finalizado-crime">
          <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i> {{$ocorrencia['titulo']}}
        </li>
      @endforeach

    </ul>
    <h4>Data e Hora da Ocorrência: <small>{{ $denuncia->data_hora}}</small></h4>
    <h4>Cep da Ocorrência: <small>{{$denuncia['cep']}}</small></h4>
    <h4>Local da Ocorrência:</h4>
    <p>Bairro - {{ $denuncia['bairro'] }}</p>
    <p>Rua - {{ $denuncia['rua'] }}</p>
    <h4>
      <a class="button button-blue" href="#">
        Download de Provas <i class="fa fa-cloud-download" aria-hidden="true"></i>
      </a>
    </h4>
    <h4>Texto Descritivo da Ocorrência
      <small>
        <a class="button button-link" data-toggle="collapse" data-target="#{{$denuncia['numero_protocolo']}}texto">
           <i class="fa fa-eye" aria-hidden="true"></i> Visualizar
        </a>
      </small>
    </h4>
    <div id="{{$denuncia['numero_protocolo']}}texto" class="collapse">
      <div class="row">
        <div class="conteiner">
          <div class="col-sm-12 col-lg-12">
            {{$denuncia['denuncia_escrita']}}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
