<div id="modalTest" class="modal fade modal-ocorrencia" role="dialog">
  <div class="ocorrencia-content">
    <div class="modal-content">
      <div class="ocorrencia-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="ocorrencia-title" id='textoModal'>Título</h4>
      </div>
      <div class="ocorrencia-body">
        <p id='desc'>Descrição do tipo de crime.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
