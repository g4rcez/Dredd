@section('title','- Painel Administrativo')
@extends('admin.layout')
@section('content')
  <div class='content-wrapper'>
      <section class="content-header">
        <h1>
          Painel de Controle<small> Menu principal</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{!!url('/dredd/_giforseg/dashboard')!!}"><i class="fa fa-dashboard"></i> Painel Principal</a></li>
        </ol>
      </section>
      <!-- Main content -->
      <section class='content'>

        <!-- Small boxes (Stat box) -->
        <div class='row'>
          @component('admin/components/boxInfo')
            @slot('background') blue @endslot
            @slot('estatistica') 150 @endslot
            @slot('percent') 25 @endslot
            @slot('titulo') Novas Ocorrências @endslot
            @slot('desc') Taxa de aumento @endslot
          @endcomponent
          @component('admin/components/boxInfo')
            @slot('background') red @endslot
            @slot('estatistica') 50 @endslot
            @slot('percent') 80 @endslot
            @slot('titulo') Crimes Graves @endslot
            @slot('desc') Taxa de Aumento @endslot
          @endcomponent
          @component('admin/components/boxInfo')
            @slot('background') yellow @endslot
            @slot('estatistica') 75 @endslot
            @slot('percent') 15 @endslot
            @slot('titulo') Ocorrências em Atraso @endslot
            @slot('desc') Taxa de aumento @endslot
          @endcomponent
        </div>
        <div class="row">
          <div class="col-sm-7">
            @include('admin.tipo_denuncia.cadastro_tipo_denuncia')
          </div>
          <div class="col-sm-5">
            @include('admin/components/calendar')
          </div>
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin/components/tipo_denuncia_modal')
@endsection
