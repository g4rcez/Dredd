<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dredd @yield('title')</title>
  {{-- <!-- Tell the browser to be responsive to screen width --> --}}
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {{-- <!-- Bootstrap 3.3.6 --> --}}
  <link rel="stylesheet" href="{!!asset('/admin/bootstrap/css/bootstrap.min.css')!!}">
  {{-- <!-- Awesome icons --> --}}
  <link rel="stylesheet" href="{!!asset('/css/font-awesome.min.css')!!}">
  {{-- <!-- Font Awesome --> --}}
  <link rel="stylesheet" href="{!!asset('/fonts/ionicons.min.css')!!}">
  {{-- Style Theme --}}
  <link rel="stylesheet" href="{!!asset('/admin/dist/css/AdminLTE.min.css')!!}">
  {{-- Blue Skin for Painel --}}
  <link rel="stylesheet" href="{!!asset('/admin/dist/css/skins/skin-blue.min.css')!!}">
  <script src="{!!asset('/js/jquery.min.js')!!}"></script>
  {{-- <!-- iCheck --> --}}
  <link rel="stylesheet" href="{!!asset('/admin/plugins/iCheck/flat/blue.css')!!}">
  {{-- <!-- Daterange picker --> --}}
  <link rel="stylesheet" href="{!!asset('/admin/plugins/daterangepicker/daterangepicker.css')!!}">
  {{-- <!-- bootstrap wysihtml5 - text editor --> --}}
  <link rel="stylesheet" href="{!!asset('/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}">
  {{-- <!-- Admin Clearfix --> --}}
  <link rel="stylesheet" href="{!!asset('/css/beautify.css')!!}">
  <link rel="stylesheet" href="{!!asset('/admin/dist/css/admin_styles.css')!!}">
</head>
@php
  $user = "Agente";
  $perm = "Root";
@endphp
<body class="hold-transition skin-blue sidebar-mini fixed">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="{!! url('/dredd/_giforseg/dashboard') !!}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>D</b>RD</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Dredd</b>Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      {{-- if required, instance the fast_navbar --}}
      @include('admin/sections/fast_navbar')
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar fixed">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <i class="fa fa-user" style="color:#fff;font-size:5em;"></i>
        </div>
        <div class="pull-left info">
          <p>{!! $user !!}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> {!!$perm!!}</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">Menu Principal</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
            <span class="pull-right-container">
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ url('/dredd/_giforseg/dashboard')}}"><i class="fa fa-circle-o"></i> Visão Geral</a></li>
            <li><a href="{{ url('/dredd/_giforseg/relatorio')}}"><i class="fa fa-circle-o"></i> Gráficos Gerais</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tabelas</span><i class="fa fa-angle-left pull-right"></i>
            <span class="pull-right-container">
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> Tabelas Gerais</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Classes de Tabelas</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-search"></i>
            <span>Pesquisas</span>
          </a>
        </li>
        <li>
          <a href="pages/widgets.html">
            <i class="fa fa-th"></i> <span>Relatórios</span> <small class="label pull-right bg-green"><i class="fa fa-clock-o"></i></small>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Gráficos</span> <i class="fa fa-angle-left pull-right"></i>
            <span class="pull-right-container">
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Controle e Cadastro</span> <i class="fa fa-angle-left pull-right"></i>
            <span class="pull-right-container">
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href=""><i class="fa fa-circle-o"></i> Usuários</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Permissão</a></li>
            <li><a href=""><i class="fa fa-circle-o"></i> Tipo de Denúncia</a></li>
          </ul>
        </li>
        <li><a href=""><i class="fa fa-book"></i> <span>Manual do Administrador</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


@yield('content')

<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="{!!asset('/admin/plugins/jQuery/jquery-2.2.3.min.js')!!}"></script>
<!-- jQuery UI 1.11.4 -->
{{-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> --}}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script> $.widget.bridge('uibutton', $.ui.button); </script>
<!-- Bootstrap 3.3.6 -->
<script src="{!!asset('/admin/bootstrap/js/bootstrap.min.js')!!}"></script>
<!-- Sparkline -->
<script src="{!!asset('/admin/plugins/sparkline/jquery.sparkline.min.js')!!}"></script>
<!-- jvectormap -->
<script src="{!!asset('/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}"></script>
<script src="{!!asset('/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!}"></script>
<!-- jQuery Knob Chart -->
<script src="{!!asset('/admin/plugins/knob/jquery.knob.js')!!}"></script>
<!-- datepicker -->
<script src="{!!asset('/admin/plugins/datepicker/bootstrap-datepicker.js')!!}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{!!asset('/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')!!}"></script>
<!-- Slimscroll -->
<script src="{!!asset('/admin/plugins/slimScroll/jquery.slimscroll.min.js')!!}"></script>
<!-- FastClick -->
<script src="{!!asset('/admin/plugins/fastclick/fastclick.js')!!}"></script>
<!-- AdminLTE App -->
<script src="{!!asset('/admin/dist/js/app.min.js')!!}"></script>
</body>
</html>
