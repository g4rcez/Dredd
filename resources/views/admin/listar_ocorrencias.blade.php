@section('title','- Admin Ocorrências')
@extends('admin.layout')
@section('content')
  <div class='content-wrapper'>
    <section class="content-header">
      <h1>
        Listagem de Ocorrências <small>Todas as ocorrências</small>
        <h4><small>Total de registros de Denúncia:</small> <strong>{{ $denuncias->count() }}</strong></h4>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dredd/_giforseg"><i class="fa fa-dashboard"></i> Painel Principal</a></li>
        <li class="active">Ocorrencias</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class='content'>
      <div class="row">
        <div class="col-md-12">
           <div class="box box-info">
            <div class="box-header with-border">
              <h1 class="box-title">Todas as Ocorrências <small>Até o dia: {{date('d/m/Y')}}</small></h1>
            </div>
             <div class="box-body with-border">
               <div class="row">
            		 @each('admin.components.ocorrencia_element', $denuncias->reverse(), 'denuncia')
            		 {{ $denuncias->links()}}
               </div>
             </div>
            </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
