@section('title','- Admin Ocorrências')
@extends('admin.layout')
@section('content')
<div class='content-wrapper'>
<script src="{!!asset('/admin/dist/js/exporting.js')!!}"></script>
<script src='{!!asset('/admin/dist/js/highcharts.src.js')!!}'></script>
<section class='content'>
  <div class="row">
    <div class="col-sm-5">
      <div id="container2"></div>
    </div>
    <div class="col-sm-7">
      <div id="container1"></div>
    </div>
  </div>
</section>
{{-- Grafico 1 --}}
<script>
  Highcharts.chart('container1', {
      chart: {
          type: 'bar'
      },
      title: {
          text: 'Histório de crimes'
      },
      subtitle: {
          text: 'Data'
      },
      xAxis: {
          categories: ['Roubos', 'Furtos', 'Agressões Físicas', 'Tráfico'],
          title: {
              text: null
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Acontecimentos(Milhares)',
              align: 'high'
          },
          labels: {
              overflow: 'justify'
          }
      },
      tooltip: {
          valueSuffix: 'mil'
      },
      plotOptions: {
          bar: {
              dataLabels: {
                  enabled: true
              }
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#ececec'),
          shadow: true
      },
      credits: {
          enabled: true
      },
      series: [{
          name: 'Junho de 2017',
          data: [107, 31, 635, 203]
      }, {
          name: 'Julho de 2017',
          data: [133, 156, 947, 408]
      }, {
          name: 'Agosto de 2017',
          data: [752, 954, 1050, 740]
      }]
  });
</script>

<script>
Highcharts.chart('container2', {
  colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
     '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
      type: 'column',
       backgroundColor: '#fefefe',
       style: { fontFamily: '\'Quicksand\', sans-serif' },
       plotBorderColor: '#606063'
    },
    title: {
      text: 'Gráfico de crimes graves',
      style: { color: '#E0E0E3', textTransform: 'uppercase', fontSize: '1.2em' }
    },
    xAxis: {
        categories: ['Estupro', 'CyberCrimes', 'Tráfico']
    },
    credits: { enabled: false },
    series: [{ name: 'Janeiro', data: [5, 3, 4]
    }, { name: 'Abril', data: [2, -2, -3]
    }, { name: 'Setembro', data: [3, 4, 4]
    }]
});
</script>

<script>
Highcharts.theme = {
   colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
      '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
   chart: {
      backgroundColor: 'none',
      style: { fontFamily: '\'Quicksand\', sans-serif' },
      plotBorderColor: '#606063'
   },
   title: {
      style: { color: '#E0E0E3', textTransform: 'uppercase', fontSize: '20px' }
   },
   subtitle: {
      style: { color: '#E0E0E3', textTransform: 'uppercase' }
   },
   xAxis: {
      gridLineColor: '#707073',
      labels: {
         style: { color: '#E0E0E3' }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      title: {
         style: {
            color: '#A0A0A3'

         }
      }
   },
   yAxis: {
      gridLineColor: '#707073',
      labels: {
         style: {
            color: '#E0E0E3'
         }
      },
      lineColor: '#707073',
      minorGridLineColor: '#505053',
      tickColor: '#707073',
      tickWidth: 1,
      title: {
         style: {
            color: '#A0A0A3'
         }
      }
   },
   tooltip: {
      backgroundColor: 'rgba(0, 0, 0, 0.85)',
      style: {
         color: '#F0F0F0'
      }
   },
   plotOptions: {
      series: {
         dataLabels: {
            color: '#B0B0B3'
         },
         marker: {
            lineColor: '#333'
         }
      },
      boxplot: {
         fillColor: '#505053'
      },
      candlestick: {
         lineColor: 'white'
      },
      errorbar: {
         color: 'white'
      }
   },
   legend: {
      itemStyle: {
         color: '#E0E0E3'
      },
      itemHoverStyle: {
         color: '#FFF'
      },
      itemHiddenStyle: {
         color: '#606063'
      }
   },
   credits: {
      style: {
         color: '#666'
      }
   },
   labels: {
      style: {
         color: '#707073'
      }
   },

   drilldown: {
      activeAxisLabelStyle: {
         color: '#F0F0F3'
      },
      activeDataLabelStyle: {
         color: '#F0F0F3'
      }
   },

   navigation: {
      buttonOptions: {
         symbolStroke: '#DDDDDD',
         theme: {
            fill: '#505053'
         }
      }
   },

   // scroll charts
   rangeSelector: {
      buttonTheme: {
         fill: '#505053',
         stroke: '#000000',
         style: {
            color: '#CCC'
         },
         states: {
            hover: {
               fill: '#707073',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            },
            select: {
               fill: '#000003',
               stroke: '#000000',
               style: {
                  color: 'white'
               }
            }
         }
      },
      inputBoxBorderColor: '#505053',
      inputStyle: {
         backgroundColor: '#333',
         color: 'silver'
      },
      labelStyle: {
         color: 'silver'
      }
   },

   navigator: {
      handles: {
         backgroundColor: '#666',
         borderColor: '#AAA'
      },
      outlineColor: '#CCC',
      maskFill: 'rgba(255,255,255,0.1)',
      series: {
         color: '#7798BF',
         lineColor: '#A6C7ED'
      },
      xAxis: {
         gridLineColor: '#505053'
      }
   },

   scrollbar: {
      barBackgroundColor: '#808083',
      barBorderColor: '#808083',
      buttonArrowColor: '#CCC',
      buttonBackgroundColor: '#606063',
      buttonBorderColor: '#606063',
      rifleColor: '#FFF',
      trackBackgroundColor: '#404043',
      trackBorderColor: '#404043'
   },

   // special colors for some of the
   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
   background2: '#505053',
   dataLabelsColor: '#B0B0B3',
   textColor: '#C0C0C0',
   contrastTextColor: '#F0F0F3',
   maskColor: 'rgba(255,255,255,0.3)'
};
// Apply the theme
Highcharts.setOptions(Highcharts.theme);
</script>
@endsection
