 <script src='{{asset('/admin/dist/js/getters_tipo_denuncia.js')}}'></script>
       <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
         {{ csrf_field() }}

         <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
           <label for="name" class="col-md-4 control-label">Nome: </label>

           <div class="col-md-6">
             <input id="login" type="text" class="form-control" name="login" value="{{ old('login') }}" required autofocus>

             @if ($errors->has('name'))
               <span class="help-block">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
             @endif
           </div>
         </div>

         <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
           <label for="email" class="col-md-4 control-label">Email: </label>

           <div class="col-md-6">
             <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

             @if ($errors->has('email'))
               <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
             @endif
           </div>
         </div>

         <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
           <label for="cpf" class="col-md-4 control-label">CPF: </label>

           <div class="col-md-6">
             <input id="cpf" type="number" class="form-control" name="cpf" value="{{ old('cpf') }}" required>

             @if ($errors->has('cpf'))
               <span class="help-block">
                                        <strong>{{ $errors->first('cpf') }}</strong>
                                    </span>
             @endif
           </div>
         </div>

         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
           <label for="password" class="col-md-4 control-label">Senha: </label>

           <div class="col-md-6">
             <input id="password" type="password" class="form-control" name="password" required>

             @if ($errors->has('password'))
               <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
             @endif
           </div>
         </div>

         <div class="form-group">
           <label for="password-confirm" class="col-md-4 control-label">Confirmar Senha: </label>

           <div class="col-md-6">
             <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
           </div>
         </div>

         <div class="form-group">
           <div class="col-md-6 col-md-offset-4">
             <button type="submit" class="btn btn-primary">
               Registrar
             </button>
           </div>
         </div>
       </form>
