@php
  date_default_timezone_set('America/Sao_Paulo');
@endphp
<div class="navbar-custom-menu">
 <ul class="nav navbar-nav">
    <!-- User Account: style can be found in dropdown.less -->
    <li class="dropdown user user-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-user"></i>
        <span class="hidden-xs">{{ $user }}</span>
      </a>
      <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
          <i class="fa fa-user" style="font-size: 5rem; color:#121212;"></i>
          <p>
            {!!$user!!}
            <small>{!!$perm!!}</small>
            Data: <strong>{!! date('d/m/Y') !!}</strong><br />
            Hora: <strong>{!! date('H:i') !!}</strong>
          </p>
          <li class="user-footer">
            <div class="pull-left">
              <a href="{!!url('/dredd/_giforseg/dashboard')!!}" class="button button-blue">Página Inicial</a>
            </div>
            <div class="pull-right">
              <a href="{!!url('/dredd/_giforseg/logout')!!}" class="button button-orange">Sair</a>
            </div>
          </li>
        </li>
        <!-- Menu Body -->

        <!-- Menu Footer-->

      </ul>
    </li>
    <!-- Control Sidebar Toggle Button -->
 </ul>
</div>
