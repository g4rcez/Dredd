 <div class="box box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">Remover Agente</h3>
      <div class="box-body">
      <form class="form-horizontal" method="post" autocomplete="off">

        <div class="form-group">
          <label for="focusedinput" class="col-sm-3 control-label">&nbsp;Login:</label>
          <div class="col-sm-8">
            <input type="text" class="form-control col-sm-12 admin-inputs form-control select2" id="tituloValor" data-toggle="tooltip" title="Informe seu nome de usuário">
          </div>
        </div>

        <div class="form-group">
          <label for="focusedinput" class="col-sm-3 control-label">&nbsp;Senha:</label>
          <div class="col-sm-8">
            <input type="password" class="form-control col-sm-12 admin-inputs form-control select2" id="tituloValor" data-toggle="tooltip" title="Informe seu MASP (numérico)">
          </div>
        </div>

        <div class="form-group">
          <label for="focusedinput" class="col-sm-3 control-label">&nbsp;Confirmar:</label>
          <div class="col-sm-9">
            <label class="switch">
              <input type="checkbox">
              <div class="slider"></div>
            </label>
          </div>
        </div>
        <button type="submit" class="button button-red pull-right">Remover</button>
      </form>
    </div>
    </div>
  </div>
