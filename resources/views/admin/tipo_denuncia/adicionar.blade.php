@section('title','- Visualizar Tipos de Crime')
@extends('admin.layout')
@section('content')
    <div class='content-wrapper'>
        <section class="content-header">
            <h1>
                Cadastro de Denúncias<small> Menu de registro</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{!!url('/dredd/_giforseg/dashboard')!!}"><i class="fa fa-dashboard"></i> Cadastro de Denúncia</a></li>
            </ol>
        </section>
        <!-- Main content -->
        <section class='content'>
            <div class="row">
                <div class="col-sm-10">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Preencha os campos para uma nova denúncia</h3><br>
                            <div class="box-body">
                                {!! Form::open(['url' => '/dredd/_giforseg/registrar/cadastrarDenuncia', 'class' => 'form-horizontal']) !!}
                                @if (count($errors) > 0)
                                    <div class="warning warning-red denuncia-errors">
                                        @foreach($errors->all() as $error)
                                            <ul class="fa-ul">
                                                <li>
                                                    <i class="fa fa-times" aria-hidden="true"></i> {!! $error !!}
                                                </li>
                                            </ul>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="form-group">
                                        <label for="focusedinput" class="col-sm-3 control-label">&nbsp;Nome:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control col-sm-12 admin-inputs form-control select2" id="tituloValor" placeholder="Furto, Roubo, Tráfico de drogas..." onchange="tituloAlter(this)" name='titulo'>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="selector1" class="col-sm-3 control-label">&nbsp;Gravidade</label>
                                        <div class="col-sm-8">
                                            <select name="gravidade" id="selectNivel" class="form-control col-sm-12 admin-inputs" onchange="trocaImagem('ocorrencia_cor', '{!! asset('/img/bars/nivel3.png') !!}')">
                                                <option class='admin-inputs' value='/img/bars/nivel1.png'>Nível 1</option>
                                                <option class='admin-inputs' value='/img/bars/nivel2.png'>Nível 2</option>
                                                <option class='admin-inputs' value='/img/bars/nivel3.png'>Nível 3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="focusedinput" class="col-sm-3 control-label pull-left">&nbsp;Descrição: </label>
                                        <div class="col-sm-8">
                                            <textarea type="text" placeholder="Comentários sobre o tipo de ocorrência" class="form-control col-sm-12 admin-inputs" style="height:80px;" id='textoDesc' onchange="descAlter(this)" name="descricao"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="focusedinput" class="col-sm-3 control-label">&nbsp;Cor:</label>
                                        <div class="col-sm-2">
                                            <input id="background-color" type="color" class="form-control admin-inputs" onchange="getRgb(this)" name='cor' />
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-offset-1 col-sm-8">
                                            <h4 style="font-weight:600;">Prévia da Caixa de Denúncia</h4>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-5 col-lg-5 col-sm-5 grid-item--width2 col-lg-offset-1 col-sm-offset-1">
                                        <div class="icon-bg" id='ocorrencia_cor' style="background-image: url({!!asset('/img/bars/nivel1.png')!!});">
                                            <h4>
                                                <a data-toggle="modal" data-target="#modalTest" class="ancor-denuncia-modal">
                                                    <h4><span class='ancor-denuncia-modal' id='tituloDenuncia'>Titulo</span></h4>
                                                </a>
                                                <span>
                                                 <label style="font-weight:400">
                                                   {!! Form::checkbox('vazio', '1') !!}
                                                     Marcar Denúncia
                                                 </label>
                                               </span>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <script src="{!!asset('/js/masonry.pkgd.min.js')!!}"></script>
                                <script>
                                    $('.grid').masonry({
                                        itemSelector: '.grid-item--width2',
                                        percentPosition: true,
                                        transitionDuration: '2s',
                                        resize: true,
                                        horizontalOrder: true
                                    });
                                </script>
                                <button type="submit" class="button button-blue pull-right">Cadastrar</button>
                                {!! Form::close() !!}
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <script src='{!!asset('/admin/dist/js/getters_tipo_denuncia.js')!!}'></script>
@endsection