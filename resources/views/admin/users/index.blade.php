@section('title','- Painel do Agente') @extends('admin.layout') @section('content')
<div class='content-wrapper'>

  <section class="content-header">
    <h1>
        Agente: {{ $agente or 'Anon'}}<small> Controle de Credenciais</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/dredd/_giforseg"><i class="fa fa-dashboard"></i> Painel Principal</a></li>
        <li class="active">Agente</li>
      </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-sm-12">
        <ul class="nav nav-tabs">
          <li class="active">
            <a data-toggle="tab" href="#atualizar">
              <h4>Atualizar Agente</h4>
            </a>
          </li>
          <li>
            <a data-toggle="tab" href="#cadastrar">
              <h4>Cadastrar Agente</h4>
            </a>
          </li>
          <li>
            <a data-toggle="tab" href="#remover">
              <h4>Remover Agente</h4>
            </a>
          </li>
        </ul>
        <div class="tab-content">
          <div id="atualizar" class="tab-pane fade in active">
            <div class='row'>
              <div class='col-sm-12'>
                @include('admin.sections.atualizar_agente')
              </div>
            </div>
          </div>
          <div id="cadastrar" class="tab-pane fade in">
            <div class='row'>
              <div class='col-sm-12'>
                @include('admin.sections.cadastro_agente')
              </div>
            </div>
          </div>
          <div id="remover" class="tab-pane fade in">
            <div class='row'>
              <div class='col-sm-12'>
                @include('admin.sections.remover_agente')
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

</div>
@endsection
