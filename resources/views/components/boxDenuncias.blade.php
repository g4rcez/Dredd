@php
  $acentos = array("á", "é", "í", "ó", "ú", "ã", "ê", "ì", "ô", "û","â", "è", "î", "õ", "ũ", "à", "è", "î", "ò", "ù");
  $vogais = array('a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u','a','e','i','o','u');
  $id = str_replace($acentos, $vogais, trim(strtolower($tipoOcorrencia->titulo)));
  $id = str_replace(" ", "", $id);
@endphp
<div class="col-xs-12 col-sm-6 col-lg-4 col-sm-4 grid-item--width2">
  <div class="icon-bg" style="background-image: url({!!asset($tipoOcorrencia->gravidade)!!}); background-color:{!! $tipoOcorrencia->cor !!}">
    <h4>
      <a data-toggle="modal" data-target="#{!! $id !!}" class="ancor-denuncia-modal">
          <h4 class='ancor-denuncia-modal'>{!! $tipoOcorrencia->titulo !!}</h4>
      </a>
      <span>
        <label style="font-weight:400">
          {!! Form::checkbox('tipos_denuncia_id[]', $tipoOcorrencia->id, old('tipos_denuncia_id')) !!}
          Marcar Denúncia
        </label>
      </span>
    </h4>
  </div>
</div>
