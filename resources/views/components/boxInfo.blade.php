<div class='col-lg-12 col-xs-12'>
 <div class='small-box bg-{{$background}}'>
    <div class='inner'>
      <h3>{{$estatistica}}</h3>
      <p>{{$titulo}}</p>
    </div>
    <div class='icon'>
      <i class='fa fa-user'></i>
    </div>
    <a href="{{$link}}" class='small-box-footer'>More info <i class='fa fa-arrow-circle-right'></i></a>
 </div>
 </div>
