<link rel="stylesheet" type="text/css" href="/time/dist/bootstrap-formhelpers.min.css">

<div class="input-group clockpicker">
    <input type="text" class="form-control" value="09:30">
    <span class="input-group-addon">
        <span class="glyphicon glyphicon-time"></span>
    </span>
</div>

<script type="text/javascript" src="/time/dist/bootstrap-formhelpers.min.js"></script>
<script type="text/javascript">
    $('.clockpicker').clockpicker();
</script>
