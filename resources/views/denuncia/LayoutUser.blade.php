<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Sistema Denúncia Giforseg Web Denuncia PJF PCJF" />
    <meta name="description" content="Sistema de web denúncia para a cidade de Juiz de Fora" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <title lang="pt-br">Dredd @yield('title')</title>
    <link rel="shortcut icon" href="{!!asset('/img/dreddIco.png')!!}" />

    <link href="{!!asset('/css/bootstrap.min.css')!!}" rel='stylesheet' type='text/css' />
    <link href="{!!asset('/css/font-awesome.min.css')!!}" rel="stylesheet">
    <script src="{!!asset('/js/jquery.min.js')!!}"></script>
    <script src="{!!asset('/js/bootstrap.min.js')!!}"></script>
    <script src="{!!asset('/js/npm.js')!!}"></script>
    <link rel="stylesheet" href="{!!asset('/css/beautify.css')!!}">
    <link rel="stylesheet" href="{!!asset('/css/main.css')!!}">

    <!--Fonts Awesome + Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans|Oxygen" rel="stylesheet">
</head>

<body>
  <header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
            <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <div class="navbar-header">
            <a class="navbar-brand" href="/">
              <img alt="Dredd" src="{!!asset('/img/dreddIco.png')!!}" width="20px">
            </a>
          </div>
        </div>
        <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
          <ul class="nav navbar-nav navbar-static-top">
              <li class="@yield('home')">
                <a href="/"><i class="fa fa-home"aria-hidden="true"></i> Página Inicial</a>
              </li>
        	    <li class="dropdown mega-dropdown class="@yield('giforseg')"">
    			        <a href="{!!url('/')!!}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-address-book" aria-hidden="true"></i> Giforseg<span class="caret"></span></a>
    				      <div class="dropdown-menu mega-dropdown-menu">
                      <div class="container-fluid">
                        <div class="tab-content">
                          <div class="tab-pane active" id="segur">
                            <ul class="nav-list list-inline">
                              <li>
                                <a href="#"><i class="fa fa-book" aria-hidden="true"></i><span>Sobre o Projeto</span></a>
                              </li>
                              <li>
                                <a href="#"><i class="fa fa-lock" aria-hidden="true"></i><span>Segurança no Uso</span></a>
                              </li>
                            </ul>
                          </div>
                              <ul class="nav nav-tabs" role="tablist">
                                 <li><a href="http://www.google.com" class="btn read_more"><span>Saiba mais</span> <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                              </ul>
            				    </div>
            				  </div>
            		  </div>
                        <li class="@yield('acompanhar')">
                          <a href="{!!url('acompanhamento')!!}"><i class="fa fa-eye" aria-hidden="true"></i><span> Acompanhamento</span></a>
                        </li>
                        <li class="@yield('denunciar')">
                          <a href="{!!url('denunciar')!!}"><i class="fa fa-phone" aria-hidden="true"></i><span> Denuncia</span></a>
                        </li>
          </li>
        </ul>
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><i class="glyphicon glyphicon-king" aria-hidden="true"></i> PJF</a></li>
                        <li><a href="#"><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i> Polícia Cívil</a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
    </nav>
  </header>
  <div class="spaces"></div>
  <div class="spaces"></div>
  <div class="spaces"></div>
  <div class="spaces"></div>
  <div class="spaces"></div>

  <div class="main-wrapper">
    @yield('content')
  </div>

  <div class="spaces"></div>

  <footer class="footer">
    <div class="container">
    <div class="spaces"></div>
    <div>
      <header class="footer_title">
        <h4>Dredd Team</h4>
        <h6>Designer, Desenvolvedor e Consultor de Segurança (referência leve ao personagem Dredd).</h6>
      </header>
      <h2>
        <a href="" class="igoogleplus"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a>&nbsp;
        &nbsp;<a href="" class="igithub"><i class="fa fa-github" aria-hidden="true"></i></a>&nbsp;
        &nbsp;<a href="" class="itelegram"><i class="fa fa-paper-plane" aria-hidden="true"></i></a>&nbsp;
        &nbsp;<a href="" class="iyoutubeplay"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
      </h2>
      <p>Não copie sem nossa permissão ou referência, a licença é GPL. Peça pelo
      código fonte aos desenvolvedores fictícios</p>
    </div>
    <div class="spaces"></div>
    <div class="spaces"></div>
    </div>
  </footer>
</body>
</html>
