@extends('denuncia.LayoutUser')
@section('acompanhar','active')
@section('content')
<div class="container-fluid">
  <section>
    <header>
      <div class="row">
        <div class="container">
          <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <h2>Protocolo 2017051785981</h2>
            <h5>Nesta seção, você poderá todas as atualizações de sua denúncia.</h5>
          </div>
        </div>
      </div>
    </header>

    <div class="spaces"></div>

    <div class="row">
      <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10">
          <table class="table table-hover table-condensed table-responsive atualiza_tabela">
            <thead class="thead-inverse">
            <tr>
              <th>Data de Atualização</th>
              <th>Feita por(unidade da Polícia)</th>
              <th>Status</th>
              <th>Novo protocolo</th>
            </tr>
          </thead>
          <tbody>
            @component('components.TableContent')
              @slot('data') 25/09/2092 @endslot
              @slot('policia') Polícia Militar @endslot
              @slot('status') Caso em Atraso @endslot
              @slot('protocolo') 2017080756431  @endslot
            @endcomponent
            @component('components.TableContent')
              @slot('data') 26/09/2092 @endslot
              @slot('policia') Polícia Fedaral @endslot
              @slot('status') Investigação Forense @endslot
              @slot('protocolo') 2017080756431  @endslot
            @endcomponent
            @component('components.TableContent')
              @slot('data') 27/09/2092 @endslot
              @slot('policia') Polícia Cibernética @endslot
              @slot('status') FingerPrint Tracker @endslot
              @slot('protocolo') 2017080756431  @endslot
            @endcomponent
          </tbody>
        </table>

        <div class="observacoes_atualizadas">
          <?php $var = "<p class=\"paragrafos\">Do ad dolor ullamco pariatur est laboris ipsum occaecat aliqua sit dolore mollit id non. Laboris culpa labore nisi est duis exercitation quis consequat dolore duis. Proident tempor minim quis ex adipisicing ullamco duis excepteur ea et commodo officia fugiat minim est.</p>"; echo $var ?>
          <h3>Download de Anexos</h3>
          <?php echo $var ?>
          <a href="#"><button class="btn btn-success">Download</button></a>
        </div>

        </div>
      </div>
    </div>
@endsection
