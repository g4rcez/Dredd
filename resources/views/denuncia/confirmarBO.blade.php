@extends('denuncia.LayoutUser')
@section('title', '- Confirmação')
@section('content')
  <div class="container-fluid">
    <section>
      <header>
        <div class="row">
          <div class="container">
            <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-10 col-lg-10">
              <h2>Ocorrência Registrada</h2>
              <h5>Sua ocorrência foi registrada com sucesso!</h5>
            </div>
          </div>
        </div>
      </header>

      <div class="spaces"></div>

      <div class="row">
        <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
          <p class="paragrafos">
           Nesta tela, irá aparecer o número de protocolo e confirmação.
           Informações de seguranças são aconselháveis a serem inseridas neste campo.
          </p>
        </div>
        </div>
      </div>

     <div class="row">
        <div class="container">
           <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
              <div class="confirma_ocorrencia make_denuncia">
                 <div class="confirma_ocorrencia_bg">
                    <div class="classe_ocorrencia_final">
                      <h2>Número de Protocolo: {{ $informacoes['hash']}}</h2>
                       <p>
                          Mensagens de segurança avisando o usuário pra não
                          fazer nenhuma ação que comprometa sua integridade.Número de confirmação
                          {{ $informacoes['numero'] }}
                       </p>

                       @component('components.confirmacao')
                         @slot('protocolo')
                          @foreach($informacoes['ocorrencia'] as $tiposDenuncia) {{ "$tiposDenuncia" }} @endforeach
                         @endslot

                         @slot('cep') {{ $informacoes['CEP'] }}  @endslot
                         @slot('addr') {{ $informacoes['local'] }}  @endslot
                         @slot('describe') {{ $informacoes['descricao'] }}  @endslot
                         @slot('hora') {{ $informacoes['hora']}}  @endslot
                         @slot('data') {{ $informacoes['data']}}  @endslot
                       @endcomponent
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <div class="row">
      <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
          <a href="/denunciar" class="btn btn-warning btn-lg pull-left" id="voltar">
            <i class="fa fa-arrow-left"> Voltar</i>
          </a>

          <a href="/denunciar" class="btn btn-info btn-lg pull-right" id="confirmar">
            <i class="fa fa-thumbs-up"> Confirmar</i>
          </a>
        </div>
      </div>
    </div>

    <div class="spaces"></div>
    <div class="spaces"></div>
    <div class="spaces"></div>
@endsection
