@php
    date_default_timezone_set('America/Sao_Paulo');
@endphp
@section('title','- Denúncia')
@section('denunciar','active')
@extends('denuncia.LayoutUser')
@section('content')
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!!asset('/css/bootstrap-clockpicker.min.css')!!}">
    <script src="{!!asset('/admin/plugins/datepicker/bootstrap-datepicker.js')!!}"></script>
    <script src="{!!asset('/admin/plugins/datepicker/bootstrap-timepicker.min.js')!!}"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!!asset('/css/bootstrap-clockpicker.min.css')!!}">
    <script src="{!!asset('/admin/plugins/datepicker/bootstrap-datepicker.js')!!}"></script>
    <script src="{!!asset('/admin/plugins/datepicker/bootstrap-timepicker.min.js')!!}"></script>
    <div class="container-fluid">
        <section>
            <header>
                <div class="row">
                    <div class="container">
                        <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <h2>Fazer uma denuncia</h2>
                            <h5>Nesta seção, você poderá fazer uma denuncia, totalmente anonima.</h5>
                        </div>
                    </div>
                </div>
            </header>
            @if (count($errors) > 0)
                <div class="row">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                            <div class="warning warning-red denuncia-errors">
                                @foreach($errors->all() as $error)
                                    <ul class="fa-ul">
                                        <li>
                                            <i class="fa fa-times" aria-hidden="true"></i> {!! $error !!}
                                        </li>
                                    </ul>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="container content">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    {!! Form::open(array('url' => '/finalizado', 'method'=>'POST', 'files' => true)) !!}
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="thumbnail adjust1">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <h2 class="media-object img-rounded img-responsive numero_ocorrencia">1</h2>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="caption">
                                                <p class="text-info lead adjust2">
                                                    Informe os tipos de sua ocorrência
                                                <p class="paragrafos">
                                                    <small>Você pode escolher mais de um tipo marcando o checkbox
                                                    </small>
                                                </p>
                                                </p>

                                                <p>
                                                </p>
                                                <blockquote class="adjust2">
                                                    <div class="panel-group" id="accordion">
                                                        <input type="hidden" name="_token"
                                                               value="{!! csrf_token() !!}"/>
                                                        <div class="row grid">
                                                            @each('components.boxDenuncias', $tipoOcorrencia, 'tipoOcorrencia')
                                                        </div>
                                                    </div>
                                                    <div class="spaces"></div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="thumbnail adjust1">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <h2 class="media-object img-rounded img-responsive numero_ocorrencia">2</h2>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="caption">
                                                <p class="text-info lead adjust2">
                                                    Dados da Ocorrência
                                                <p class="paragrafos">
                                                    <small>Nesta seção, você deverá informar dados pertinentes a
                                                        ocorrência
                                                    </small>
                                                </p>
                                                </p>
                                                <blockquote class="adjust2">
                                                    <label for="data">Data da Ocorrência:</label>
                                                    <div class="input-group">
                                                        <script>
                                                            $(document).ready(function () {
                                                                $('#data').datepicker({
                                                                    format: "dd/mm/yyyy",
                                                                    language: "pt-BR"
                                                                });
                                                            });
                                                        </script>
                                                        <input data-date-end-date="0d" type="text" id="data"
                                                               class="form-control data-widget"
                                                               placeholder="{!! date('d/m/y') !!}" name="data"
                                                               value="{!! old('data') !!}"/>
                                                        <label class="input-group-addon btn" for="data">
                                                            <span class="fa fa-calendar"></span>
                                                        </label>
                                                    </div>

                                                    <br/>

                                                    <label for="hora">Horário da Ocorrência:</label>
                                                    <div class="input-group clockpicker" data-placement='left'
                                                         data-align='top' data-autoclose='true'>
                                                        <input type="text" id='hora' class="form-control" name='hora'
                                                               placeholder="{!! date('G:i') !!}"
                                                               value="{!! old('hora') !!}"/>
                                                        <span class="input-group-addon">
                             <span class="glyphicon glyphicon-time"></span>
                         </span>
                                                    </div>
                                                    <br/>

                                                    <label for="cep">CEP:</label>
                                                    <div class="input-group">
                                                        <input name="cep" type="text" id="cep" size="10" maxlength="9"
                                                               class="make_denuncia form-control"
                                                               value="{!! old('cep') !!}"
                                                               onblur="pesquisacep(this.value);"
                                                               placeholder="00000-00"/>
                                                        <label class="input-group-addon btn" for="cep">
                                                            <span class="fa fa-envelope-open"></span>
                                                        </label>
                                                    </div>
                                                    <br/>

                                                    <label for="rua">Rua:</label>
                                                    <div class="input-group">
                                                        <input name="rua" type="text" id="rua" value="" size="10"
                                                               maxlength="9"
                                                               class="make_denuncia form-control" value="" disabled=""/>
                                                        <label class="input-group-addon btn" for="cep">
                                                            <span class="fa fa-road"></span>
                                                        </label>
                                                    </div>
                                                    <br/>

                                                    <label for="rua">Bairro:</label>
                                                    <div class="input-group">
                                                        <input name="bairro" type="text" id="bairro" value="" size="10"
                                                               maxlength="9"
                                                               class="make_denuncia form-control" value="" disabled=""/>
                                                        <label class="input-group-addon btn" for="cep">
                                                            <span class="fa fa-building"></span>
                                                        </label>
                                                    </div>
                                                    <br/>

                                                    <label for="rua">Cidade:</label>
                                                    <div class="input-group">
                                                        <input name="cidade" type="text" id="cidade" value="" size="10" maxlength="9"
                                                               class="make_denuncia form-control" value="" disabled=""/>
                                                        <label class="input-group-addon btn" for="cep">
                                                            <span class="fa fa-map-marker"></span>
                                                        </label>
                                                    </div><br  />
                                                    <div class="spaces"></div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="thumbnail adjust1">
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                            <h2 class="media-object img-rounded img-responsive numero_ocorrencia">3</h2>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-xs-12">
                                            <div class="caption">
                                                <p class="text-info lead adjust2">
                                                    Dados subjetivos da ocorrência
                                                <p class="paragrafos">
                                                    <small>Nesta seção você irá informar dados que observou
                                                        para ajudar a polícia a identificar melhor sua ocorrência
                                                    </small>
                                                </p>
                                                </p>
                                                <blockquote class="adjust2">
                                                    <p>
                                                        Aqui, você deverá informar o máximo de dados sobre sua
                                                        denúncia e de forma simplificada, para que possamos
                                                        identificar melhor a ocorrência. Máximo de 2500 caracteres.<br />
                                                        <small>Não dê informações que possam indicar quem você é, para que
                                                            seu anonimato seja preservado.</small>
                                                    </p>
                                                    <textarea placeholder="Digite sua denúncia" rows="5" maxlength="2500" cols="70" class="form-control" name="texto" id="denuncia">{!! old('texto') !!}</textarea>

                                                    Caracteres restantes: <span id="charNum">2500</span>
                                                    <div class="spaces"></div>

                                                    <div class="form-group">

                                                        <label for="password">Senha:</label>
                                                        <div class="input-group">
                                                            <input name="password" type="password" id="password" class="make_denuncia form-control" required'/>
                                                            <label class="input-group-addon btn" for="password" id='view_password'>
                                                                <span class="fa fa-eye"></span>
                                                            </label>
                                                        </div>
                                                        <small>Sua senha deve conter <strong>9 caracteres</strong>.</small>
                                                        <br />


                                                        <label for="password_confirmation">Confirmar senha:</label>
                                                        <div class="input-group">
                                                            <input name="password_confirmation" type="password" id="password_confirmation" class="make_denuncia form-control" required/>
                                                            <label class="input-group-addon btn" for="password_confirmation" id='view_password_confirm'>
                                                                <span class="fa fa-eye"></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="spaces"></div>

                                                    <div class="form-group">
                                                        <label for="fileupload" class="make_denuncia">Enviar de
                                                            Anexo</label><br/>
                                                        <span class="bnt btn-success fileinput-button">
                                                            <i class="glyphicon glyphicon-plus"></i>
                                                            <span>Selecione os arquivos...</span>
                                                            {!! Form::file('arquivos[]', array('multiple'=>true, 'id'=>'fileupload')) !!}
                                                        </span>
                                                        <span id="numberFiles"></span>
                                                        <br/>
                                                        <strong><p class="help-block" id="refreshFiles"></p></strong>
                                                        <div id="selectedFiles">
                                                        </div>
                                                        <p class="help-block">
                                                            Você pode enviar fotos, vídeos, documentos
                                                            e áudios. Limite do arquivo: 5 MB
                                                        </p>
                                                    </div>
                                                    <hr />
                                                    <h3>Finalizar Formulário</h3>
                                                    <p style="text-indent:1.5em">
                                                        Na próxima tela, você irá confirmar todos os dados informados
                                                        e encerrar sua denúncia. Também irá receber um número
                                                        de protocolo para acompanhar a sua denúncia, este
                                                        número não deve ser compartilhado com ninguem.
                                                    </p>
                                                    <input type="submit" class="button button-blue" value="Enviar Ocorrência" />
                                                    <div class="spaces"></div>
                                                    <div class="spaces"></div>
                                                </blockquote>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
            </div>
            @each('components.modals', $tipoOcorrencia, 'tipoOcorrencia')
        </section>

        <link rel="stylesheet" href="{!!asset('/admin/plugins/datepicker/datepicker3.css')!!}" />
        <script type="text/javascript" src="{!!asset('/js/bootstrap-clockpicker.min.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('/js/jquery.mask.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('js/verificaCep.js')!!}"></script>
        <script src="{!!asset('/js/masonry.pkgd.min.js')!!}"></script>
        <script type="text/javascript" src="{!!asset('/js/characterCount.js')!!}"></script>
        <script type="text/javascript">
            $('.clockpicker').clockpicker({
                placement: 'bottom',
                align: 'left',
            });
            $('.carousel').carousel({
                pause: true,
                interval: false
            });
            $('.grid').masonry({
                itemSelector: '.grid-item--width2',
                percentPosition: true,
                transitionDuration: '1.5s',
                resize: true,
                horizontalOrder: true
            });
            var input = document.querySelector('#password');
            var img = document.querySelector('#view_password');
            img.addEventListener('click', function () {
                input.type = input.type == 'text' ? 'password' : 'text';
            });

            var input2 = document.querySelector('#password_confirmation');
            var img2 = document.querySelector('#view_password_confirm');
            img2.addEventListener('click', function () {
                input2.type = input2.type == 'text' ? 'password' : 'text';
            });
        </script>
        <script src="{!! asset('/js/fileUpload.js') !!}"></script>
@endsection
