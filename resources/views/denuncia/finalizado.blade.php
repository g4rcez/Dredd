@extends('denuncia.LayoutUser')
@section('title','- Denúncia')
@section('acompanhar','active')
@section('content')
    <div class="container-fluid">
        <section>
            <header>
                <div class="row">
                    <div class="container">
                        <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-10 col-lg-10">
                            <h2>Ocorrência Registrada</h2>
                            <h5>Sua ocorrência foi registrada com sucesso!</h5>
                        </div>
                    </div>
                </div>
            </header>

            <div class="spaces"></div>

            <div class="row">
                <div class="container">
                    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                        <p class="paragrafos">
                            Guarde o numero do Protocolo e o numero de confirmação, com ambos será possivel acompanhar
                            sua denuncia.
                        </p>
                        <p class="paragrafos">
                            Sua vida depende disso!
                        </p>
                    </div>
                </div>
            </div>

            <article class="">
                <div class="row">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                            <div class="finalizado-box">
                                <h2>
                                    Protocolo: {!! $denuncia->protocolo !!}
                                </h2>
                                <h3>Tipos de Crime da Denúncia</h3>
                                <ul class="fa-ul">
                                    @foreach ($denuncia->denunciasAssociadas as $crime)
                                        <li class="finalizado-crime">
                                            <i class="fa-li fa fa-long-arrow-right"
                                               aria-hidden="true"></i>{!! $crime->titulo !!}
                                        </li>
                                    @endforeach
                                </ul>

                                <h3>Data e Hora da Ocorrência</h3>
                                <ul class="fa-ul">
                                    <li class="finalizado-crime">
                                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>
                                        Data e Hora: {!! $denuncia->data_hora->format('d/m/Y H:i') !!}
                                    </li>
                                </ul>
                                <h3>Local da Ocorrência</h3>
                                <ul class="fa-ul">
                                    <li class="finalizado-crime">
                                        <i class="fa-li fa fa-long-arrow-right"
                                           aria-hidden="true"></i>CEP: {!! $denuncia->cep !!}
                                    </li>
                                    <li class="finalizado-crime">
                                        <i class="fa-li fa fa-long-arrow-right"
                                           aria-hidden="true"></i>Cidade: {!! $denuncia->cidade !!}
                                    </li>
                                    <li class="finalizado-crime">
                                        <i class="fa-li fa fa-long-arrow-right"
                                           aria-hidden="true"></i>Bairro: {!! $denuncia->bairro !!}
                                    </li>
                                    <li class="finalizado-crime">
                                        <i class="fa-li fa fa-long-arrow-right"
                                           aria-hidden="true"></i>Rua: {!! $denuncia->rua !!}
                                    </li>
                                </ul>
                                <h3>Denúncia Descritiva</h3>
                                <ul class="fa-ul">
                                    <li class="finalizado-crime">
                                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>
                                        <p style="word-wrap: break-word;">
                                            {!! htmlentities($denuncia->texto) !!}
                                        </p>
                                    </li>
                                </ul>
                                <h3>Arquivos Informado</h3>
                                <ul class="fa-ul">
                                    @php $numero = 1 @endphp
                                    @foreach ($denuncia->arquivos as $arquivo)
                                        <li class="finalizado-crime">
                                            <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>
                                            <a href='{!! $arquivo->path.'.'.$arquivo->extensao !!}'
                                               class='button button-link'>Arquivo - {!! $numero !!}<span
                                                        class='file-archive-o'></span></a>
                                        </li>
                                        @php $numero+=1 @endphp
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>

        <div class="spaces"></div>
        <div class="spaces"></div>
        <div class="spaces"></div>
@endsection
