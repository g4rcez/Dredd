@extends('denuncia.LayoutUser')
@section('title', '- Bem Vindo')
@section('home','active')
@section('content')
<div class="conteiner-fluid">
  <section>
    <header>
      <div class="row">
        <div class="conteiner">
          <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-10 col-lg-10">
            <h1>Sistema de Web Denúncia</h1>
            <h5>Você não precisa mais ligar, e o anonimato é ainda mais garantido.</h5>
          </div>
        </div>
      </div>
    </header>

    <div class="spaces"></div>

    <div class="row">
      <div class="conteiner">
        <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-10 col-lg-10">
          <p class="paragrafos">
            Apelidado de Dredd(referência nerd), o sistema de web denúncia funciona
            para que você possa fazer denúncias online, de forma prática, segura
            e anônima. Os desenvolvedores não possuem acesso a nada após a conclusão
            do desenvolvimento e implementação do sistema.
          </p>

          <p class="paragrafos">
            Flexibilidade. Alguns módulos do sistema são flexiveis de acordo com a
            necessidade da cidade em que for implementado. Os módulos de permissão
            do usuário, cadastro de tipos de ocorrência e novos usuários, são geridos
            pelo Administrador, este que pode adicionar e modificar módulos do sistema.
          </p>

          <div class="row">
            <div class="conteiner">
              <div class="col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <header>
                  <h3>Fazer uma ocorrência</h3>
                </header>
                <p class="paragrafos">
                  Você poderá fazer uma denuncia, totalmente anônima, através
                  de seu navegador, seja pelo computador ou smartphone(#verificar
                  responsividade)
                </p>

                <div class="termo_de_uso">
                  <div class="texto_uso">
                    <?php
                      $termo_de_uso_txt="Aqui é aconselhável que fique disponível
                      algumas dicas de segurança, uns artigos da lei que dão proteção
                      ao usuário...Poderá ser separado em tópicos para que fique de
                      fácil leitura e que também possa ser fácil a leituras em
                      smartphones.";
                      echo "<p> $termo_de_uso_txt </p>";
                    ?>
                  </div>
                </div>

                <div class="spaces"></div>

                <form class="">
                    <div class="form-group">
                      <label class="radio_label"><input type="checkbox" id="inputTermos"> Li e concordo com todos os termos</label><br />
                      <span>
                        <button type="button" class="button button-orange" data-toggle="modal" data-target="#myModal">Termos de Uso</button>
                        <button type="button" class="button button-blue" id="bntTermos">Aceitar e Continuar</button>
                      </span>
                    </div>
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Termos de uso</h4>
                          </div>
                          <div class="modal-body modal_body_terms">
                            <?php echo "<p> $termo_de_uso_txt</p>"; ?>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="button button-blue" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>

              </div>
              <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                <header>
                  <h3>Consultar uma ocorrência</h3>
                </header>
                <p class="paragrafos">
                  O seu número de protocolo foi gerado através de nosso algoritmo,
                  para sua segurança e anonimato, não compartilhe com ninguem.
                  Seu protocolo é instranferível, cuidado.
                </p>
                  @if (count($errors) > 0)
                      <div class="row">
                          <div class="conteiner">
                              <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
                                  <div class="warning warning-red denuncia-errors">
                                      @foreach($errors->all() as $error)
                                          <ul class="fa-ul">
                                              <li>
                                                  <i class="fa fa-times" aria-hidden="true"></i> {!! $error !!}
                                              </li>
                                          </ul>
                                      @endforeach
                                  </div>
                              </div>
                          </div>
                      </div>
                  @endif

                  {!! Form::open(['url' => '/acompanhamento']) !!}
                      <div class="form-group">
                        <label for="protocolo" class="radio_label">Número de Protocolo:</label>
                        <input name="protocolo" value="{!! old('protocolo')!!}" type="number" class="form-control number-wrapper" id="protocolo" placeholder="Digite seu protocolo...13 digitos">
                      </div>
                      <div class="form-group">
                        <label for="password" class="radio_label">Senha:</label>
                        <input name="password" value="{!! old('password') !!}" type="password" class="form-control number-wrapper" id="password" placeholder="Digite seu número de confirmação...">
                      </div>
                    <h6>
                      * Lembre-se de que nunca se pode compartilhar esse número de protocolo e senha...
                    </h6>
                    <button type="submit" class="button button-blue">Consultar</button>
                  {!! Form::close() !!}
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </section>

  <div class="spaces"></div>
  <div class="spaces"></div>
  <div class="spaces"></div>
    <script>
        $("#bntTermos").click(function() {
           if($("#inputTermos").checked){
               //url = "http://www.rapidtables.com/web/dev/jquery-redirect.htm";
               //$(location).attr("href", url);
           }
        });
    </script>
@endsection
