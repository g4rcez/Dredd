@extends('denuncia.LayoutUser')
@section('acompanhar','active')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10">
        <header>
          <h2>Pedido de acompanhamento</h2>
          <p class="paragrafos">
            Nesta tela, você pode fazer o pedido de acompanhamento da sua denúncia.
            Basta informar seu número de protocolo e o número de confirmação.
            <b>Número de protocolo: XXXXYYZZZAAAAA</b>
          </p>
        </header>
      </div>
    </div>
  </div>

  </div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10">
        @if (count($errors) > 0)
            <div class="row">
                <div class="conteiner">
                    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10">
                        <div class="warning warning-red denuncia-errors">
                            @foreach($errors->all() as $error)
                                <ul class="fa-ul">
                                    <li>
                                        <i class="fa fa-times" aria-hidden="true"></i> {!! $error !!}
                                    </li>
                                </ul>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {!! Form::open(['url' => '/acompanhamento']) !!}
            <div class="form-group">
              <label for="protocolo" class="radio_label">Número de Protocolo:</label>
              <input name="protocolo" value="{!! old('protocolo')!!}" type="number" class="form-control" id="protocolo" placeholder="Digite seu protocolo...13 digitos">
            </div>
            <div class="form-group">
              <label for="password" class="radio_label">Senha:</label>
              <input name="password" value="{!! old('password')!!}" type="password" class="form-control" id="password" placeholder="Digite sua senha...">
            </div>
          <h6>
            * Lembre-se de que nunca se pode compartilhar esse protocolo e número de confirmação...
          </h6>
          <button type="submit" class="button button-blue">Consultar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
