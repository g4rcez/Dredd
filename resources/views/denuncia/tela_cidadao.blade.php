@php
  date_default_timezone_set('America/Sao_Paulo');
@endphp
@section('title','- Acompanhamento:')
@section('acompanhar','active')
@extends('denuncia.LayoutUser')
@section('content')
<div class="row">
  <div class="container">
    <div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10">
      <h1 class="text-center">Menu do Cidadão</h1>
      <p class="paragrafos">
        Mussum Ipsum, cacilds vidis litro abertis. Si num tem leite então bota uma pinga aí cumpadi! Manduma pindureta quium dia nois paga. Mais vale um bebadis conhecidiss, que um alcoolatra anonimis. Leite de capivaris, leite de mula manquis sem cabeça.
        Mauris nec dolor in eros commodo tempor. Aenean aliquam molestie leo, vitae iaculis nisl. Em pé sem cair, deitado sem dormir, sentado sem cochilar e fazendo pose. Detraxit consequat et quo num tendi nada. Delegadis gente finis, bibendum egestas augue arcu ut est.
      </p>
      <ul class="nav nav-tabs nav-justified ocorrencia-tabs">
        <div class="espacos"></div>
        <li class="active">
          <a data-toggle="tab" href="#ocorrencia">
            <h4>Visualizar Ocorrência</h4>
          </a>
        </li>
        <li>
          <a data-toggle="tab" href="#acompanhamento">
            <h4>Acompanhamento</h4>
          </a>
        </li>
      </ul>
      <div class="tab-content">
        <div id="ocorrencia" class="tab-pane fade in active">
          <article>
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                  <div class="finalizado-box">
                    <h2>
                      Protocolo: {{ $denunciaPesquisada->protocolo }}
                    </h2>
                    <h3>Tipos de Crime da Denúncia</h3>
                    <ul class="fa-ul">
                      @foreach ($denunciaPesquisada->denunciasAssociadas as $crime)
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>{{ $crime->titulo }}
                      </li>
                      @endforeach
                    </ul>

                    <h3>Data e Hora da Ocorrência</h3>
                    <ul class="fa-ul">
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i> Data e Hora: {{ $denunciaPesquisada->data_hora->format('d/m/Y G:i') }}
                      </li>
                    </ul>
                    <h3>Local da Ocorrência</h3>
                    <ul class="fa-ul">
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>CEP: {{ $denunciaPesquisada->cep }}
                      </li>
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>Cidade: {{ $denunciaPesquisada->cidade }}
                      </li>
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>Bairro: {{ $denunciaPesquisada->bairro }}
                      </li>
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>Rua: {{ $denunciaPesquisada->rua }}
                      </li>
                    </ul>
                    <h3>Denúncia Descritiva</h3>
                    <ul class="fa-ul">
                      <li class="finalizado-crime">
                        <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>
                        <p style="word-wrap: break-word;">
                          {!! $denunciaPesquisada->texto !!}
                        </p>
                      </li>
                    </ul>
                    <h3>Arquivos Informado</h3>
                    <ul class="fa-ul">
                      @php $numero = 1 @endphp
                      @foreach ($denunciaPesquisada->arquivos as $arquivo)
                        <li class="finalizado-crime">
                          <i class="fa-li fa fa-long-arrow-right" aria-hidden="true"></i>
                          <a href='{!! $arquivo->path.'.'.$arquivo->extensao !!}'
                             class='button button-link'>Arquivo - {!! $numero !!}<span
                                    class='file-archive-o'></span></a>
                        </li>
                        @php $numero+=1 @endphp
                      @endforeach
                    </ul>
                    <div class="spaces"></div>
                    <div class="spaces"></div>
                    <div class="spaces"></div>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
        <div id="acompanhamento" class="tab-pane fade">

          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-hover table-condensed table-responsive atualiza_tabela">
                  <thead class="thead-inverse">
                    <tr>
                      <th>Data de Atualização</th>
                      <th>Status</th>
                      <th>Descrição</th>
                    </tr>
                  </thead>
                  <tbody>
                  <tr>
                      <th scope="row">{!! date('d/m/Y') !!}</th>
                      <td>Loirão Barbudo</td>
                      <td>O importante é o fluxo de informação</td>
                  </tr>
                  </tbody>
                </table>

                <div class="spaces"></div>

              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
