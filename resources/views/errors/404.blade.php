@extends('denuncia.LayoutUser')
@section('title', '- Not Found 404')

@section('content')
  <div class="row">
    <div class="conteiner">
      <div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
        <div class="centraliza">
          <h1>404 Error. Not Found</h1>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="conteiner">
      <div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
        <p class="error-informacao">
          Erro ao encontrar a página solicitada
        </p>
        <p class="error-informacao">
          @inject('request', 'Request')
          <small>[{{ request::url() }}] não encontrada.</small>
        </p>
      </div>
    </div>
  </div>
@endsection
