<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Rotas de Ocorrência
Route::get('/', 'IndexController@boasVindas');
Route::get('/denunciar', 'DenunciaController@denunciar');
//Route::get('/finalizado/{id}', 'DenunciaController@encerrarOcorrencia');
Route::post('/finalizado', 'DenunciaController@encaminharOcorrencia');
Route::get('/acompanhamento', 'DenunciaController@pedirAcompanhamento');
Route::post('/acompanhamento', 'DenunciaController@acompanhamento');

// Rotas de errors apontados na URI
Route::get('/404', 'ErrorController@Erro404');

// Rotas do administrador do sistema
Route::group(['prefix' => '/dredd/_giforseg'], function () {
    Route::get('/login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm' ]);
    Route::post('/login', [ 'as' => '', 'uses' => 'Auth\LoginController@login' ]);

    Route::post('password/email', [ 'as' => 'password.email',  'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail' ]);
    Route::get('password/reset', [ 'as' => 'password.request',  'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm' ]);
    Route::post('password/reset', [ 'as' => '',  'uses' => 'Auth\ResetPasswordController@reset' ]);
    Route::get('password/reset/{token}', [ 'as' => 'password.reset',  'uses' => 'Auth\ResetPasswordController@showResetForm' ]);

    Route::get('/registrar', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('/registrar', ['as' => '', 'uses' => 'Auth\RegisterController@register']);

    Route::group([ 'middleware' => ['auth'] ], function () {
        Route::get('/dashboard', 'Admin\AdminController@index')->name('home');

        Route::group(['prefix' => '/denuncia'], function () {
            Route::get('/','Admin\CrudDenunciaController@index');
            Route::get('/cadastrar','Admin\CrudDenunciaController@view');
            Route::post('/cadastrar','Admin\CrudDenunciaController@create');
            Route::get('/editar','Admin\CrudDenunciaController@update');
        });


        Route::get('/ocorrencias', 'Admin\AdminController@ocorrencias');
        Route::get('/relatorio', 'Admin\AdminController@relatorio');
        Route::get('/agente', 'Admin\AdminController@controleAgente');

        Route::post('/logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
        Route::get('/logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    });
});
